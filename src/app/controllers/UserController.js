import User from '../models/User';
import token from '../../services/token';
import sqlServerUtils from '../../services/sqlserver';
class UserController {
  login = async (req, res) => {
    let { username, password, ipAcesso } = req.body;
    if (username && password) {
      global.sql.request()
        .input('in_usuUsuario', username)
        .input('in_usuSenha', password)
        .input('in_ipAcesso', ipAcesso || req.connection.remoteAddress || '127.0.0.1')
        .execute('sp_Usuarios_C4')
        .then((result) => handleAuth(result.recordset[0]))
        .catch(err => res.status(403).json(sqlServerUtils.errorExtractor(err)));
    } else {
      res.sendStatus(400);
    }

    const handleAuth = (result) => {
      if (!result?.token) res.sendStatus(403);
      result.token = token.encrypt(result.token);
      res.send(result);
    }
  }

  me = async (req, res) => {
    const userToken = res.locals.decryptedToken;
    global.sql.request()
      .input('in_token', userToken)
      .execute('APP_VENDA_SELECT_USUARIO')
      .then((result) => handleResult(result.recordset[0]))
      .catch(err => { res.status(403).json(err) });

    const handleResult = (result) => {
      res.send(result);
    }
  }



  async index(req, res) {
    try {
      const userFind = await User.find();
      return res.send(userFind);
    } catch (error) {
      return res.status(500).send({ index: false, error });
    }
  }

  async show(req, res) {
    const { id } = req.params;

    try {
      const userFind = await User.findById(id);
      if (!userFind)
        return res.send({ error: 'Usuário não existe na base de dados' });

      return res.send(userFind);
    } catch (error) {
      return res.status(500).send({ show: false, error });
    }
  }

  async store(req, res) {
    const userExists = await User.findOne({ email: req.body.email });

    if (userExists) {
      return res.status(400).json({ error: 'Usuário já existente' });
    }

    try {
      const { user, password, name, cpf, email } = await User.create(req.body);

      return res.json({
        user,
        password,
        name,
        cpf,
        email,
      });
    } catch (error) {
      return res.status(500).send({ store: false, error });
    }
  }

  async update(req, res) {
    const { id } = req.params;
    const { user, name, cpf, email } = req.body;

    try {
      const userModified = {
        user,
        name,
        cpf,
        email,
      };

      const options = { new: true };
      await User.findByIdAndUpdate(id, userModified, options);

      return res.send({ message: 'Usuário atualizado com sucesso' });
    } catch (error) {
      return res
        .status(500)
        .send({ message: 'Não foi possível atualizar o usuário' });
    }
  }

  async delete(req, res) {
    const { id } = req.params;
    if (!id) return res.send({ delete: false, id: null });

    try {
      const userFind = await User.findByIdAndRemove(id);
      if (!userFind) return res.send({ message: 'Usuário não existente' });

      return res.send({ message: 'Usuário deletado com sucesso' });
    } catch (error) {
      return res
        .status(500)
        .send({ message: 'Não foi possível deletar o usuário', error });
    }
  }
}

export default new UserController();
