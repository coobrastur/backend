import { Router } from 'express';
import isAuth from '../app/middlewares/isAuth';
import ContractController from '../app/controllers/ContractController';
const contractRoutes = Router();

/**
 * @swagger
 * tags:
 *   name: contratos
 *   description: As contratos inseridas pelo app/site
 */

contractRoutes.get('/index/:id', ContractController.index);
contractRoutes.get('/show/:id', ContractController.show);
contractRoutes.get('/showAll', ContractController.showAll);
contractRoutes.post('/store/:id', isAuth, ContractController.store);
contractRoutes.put('/update/:id', ContractController.update);
contractRoutes.delete('/delete/:id', ContractController.delete);

/**
 * @swagger
 * /contratos/my:
 *   get:
 *     summary: Meus contratos
 *     tags: [contratos]
 *     responses:
 *       200:
 *         description: A lista de contratos
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/definitions/contrato-minified'
 *       400:
 *         description: Bad request
 */
contractRoutes.get('/my', ContractController.my);

/**
* @swagger
* /contratos/all:
*   get:
*     summary: Usuário Nível 2=Todas as vendas da empresa do usuário | Usuário Nível 3=Todas as vendas do sistema.
*     tags: [contratos]
*     responses:
*       200:
*         description: A lista de contratos
*         content:
*           application/json:
*             schema:
*               type: array
*               items:
*                 $ref: '#/definitions/contrato-minified'
*       400:
*         description: Bad request
*/
contractRoutes.get('/all', ContractController.all);

/**
 * @swagger
 * /contratos/new:
 *   post:
 *     summary: Insere novo Contrato
 *     tags: [contratos]
 *     requestBody:
 *       content:
 *         application/json:
 *             schema:
 *               type: object
 *               $ref: '#/definitions/contrato-new'
 *     responses:
 *       200:
 *         description: Contrato inserido, ID retornado.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 id:
 *                  type: number
 *                  description: O ID gerado automaticamente.
 *       400:
 *         description: Bad request
 */
contractRoutes
  .post('/new', ContractController.store)


/**
 * @swagger
 * /contratos/{id}:
 *   get:
 *     summary: Obtém Contrato por ID
 *     parameters:
 *     - in: path
 *       name: id
 *       required: true
 *       description: ID numérico da Contrato para ser retornado.
 *       example: 48
 *       schema:
 *         type: integer
 *     tags: [contratos]
 *     responses:
 *       200:
 *         description: Contrato encontrado e retornado
 *       404:
 *         description: Contrato não encontrado
 *       400:
 *         description: Id inválido (Não numérico)
 *   delete:
 *     summary: Deleta Contrato por ID
 *     tags: [contratos]
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         example: 48
 *         description: ID numérico do Contrato para ser deletado.
 *         schema:
 *           type: integer
 *     responses:
 *       200:
 *         description: Contrato encontrado e deletado
 *       404:
 *         description: Contrato não encontrado
 *       400:
 *         description: Id inválido (Não numérico)
 */
contractRoutes.route('/:id')
  .get(ContractController.getById)
  .delete(ContractController.deleteById);



/**
 * @swagger
 * /contratos/edit:
 *   patch:
 *     summary: Edita rascunho de contrato por ID
 *     tags: [contratos]
 *     requestBody:
 *       content:
 *         application/json:
 *             schema:
 *               type: object
 *               $ref: '#/definitions/contrato-patch'
 *     responses:
 *       200:
 *         description: Contrato encontrado e editado
 *       404:
 *         description: Contrato não encontrado
 *       400:
 *         description: Id inválido (Não numérico)
 */
contractRoutes
  .patch('/edit', ContractController.patchById);

/**
 * @swagger
 * /contratos/insert-payment/credit:
 *   post:
 *     summary: Insere os dados de pagamento no contrato (tipo crédito)
 *     tags: [contratos]
 *     requestBody:
 *       content:
 *         application/json:
 *             schema:
 *               type: object
 *               $ref: '#/definitions/payment-credit'
 *     responses:
 *       200:
 *         description: Dados de pagamento inseridos com sucesso.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 id:
 *                  type: number
 *                  description: O ID gerado automaticamente.
 *       400:
 *         description: Bad request
 */
contractRoutes.post('/insert-payment/credit', ContractController.insertPaymentCredit);

/**
 * @swagger
 * /contratos/insert-payment/debit:
 *   post:
 *     summary: Insere os dados de pagamento no contrato (tipo débito)
 *     tags: [contratos]
 *     requestBody:
 *       content:
 *         application/json:
 *             schema:
 *               type: object
 *               $ref: '#/definitions/payment-debit'
 *     responses:
 *       200:
 *         description: Dados de pagamento inseridos com sucesso.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 id:
 *                  type: number
 *                  description: O ID gerado automaticamente.
 *       400:
 *         description: Bad request
 */
contractRoutes.post('/insert-payment/debit', ContractController.insertPaymentDebit);

export default contractRoutes;
