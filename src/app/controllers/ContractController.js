import Contract from '../models/Contract';
import utils from '../middlewares/utils';
import { parse, isValid, format } from 'date-fns';
import { ptBR } from 'date-fns/locale';
import sql from 'mssql';
import sqlServerUtils from '../../services/sqlserver';
import token from '../../services/token'
import D4signController from './D4signController';
import chalk from 'chalk';

import { setD4SignStatus, enviarVenda } from '../../services/d4sign';

class ContractController {

  my = async (req, res) => {
    const token = res.locals.decryptedToken;
    if (token) {
      global.sql.request()
        .input('in_token', token)
        .execute('APP_VENDA_SELECT_BY_TOKEN')
        .then((result) => res.send(result.recordset))
        .catch(err => res.status(403).json(sqlServerUtils.errorExtractor(err)));
    } else {
      res.sendStatus(400);
    }
  }

  getById = async (req, res) => {
    const token = res.locals.decryptedToken;
    const { id } = req.params;

    if (isNaN(id)) {
      res.sendStatus(400)
    }

    if (token) {
      global.sql.request()
        .input('in_token', token)
        .input('in_appvenda', id)
        .input('seguranca', 1)
        .execute('APP_VENDA_SELECT_BY_ID')
        .then((result) => { handleGet(result) })
        .catch(err => res.status(403).json(sqlServerUtils.errorExtractor(err)));
    } else {
      res.sendStatus(400);
    }
    const handleGet = (result) => {
      if (result.recordset.length === 0) res.sendStatus(404);
      res.send(result.recordset[0]);
    }
  }

  deleteById = async (req, res) => {
    const token = res.locals.decryptedToken;
    const { id } = req.params;

    if (isNaN(id)) {
      res.sendStatus(400)
    }

    if (token) {
      global.sql.request()
        .input('in_token', token)
        .input('in_appvenda', id)
        .execute('APP_VENDA_DELETE_BY_ID')
        .then((result) => { handleDelete(result) })
        .catch(err => res.status(403).json(sqlServerUtils.errorExtractor(err)));
    } else {
      res.sendStatus(400);
    }

    const handleDelete = (result) => {
      if (result.recordset[0].count === 0) res.sendStatus(404);
      res.sendStatus(200);
    }

  }

  async index(req, res) {
    const { id } = req.params;

    try {
      const contractsFind = await Contract.find({ createdById: id });
      return res.send(contractsFind);
    } catch (error) {
      return res.status(500).send({
        message: 'Algum erro interno ocorreu. Tente novamente mais tarde',
        error,
      });
    }
  }

  async show(req, res) {
    const { id } = req.params;

    try {
      const contractFind = await Contract.findById(id).populate('createdById');

      return res.send(contractFind);
    } catch (error) {
      return res.status(500).send({
        message: 'Algum erro interno ocorreu. Tente novamente mais tarde',
      });
    }
  }

  async showAll(req, res) {
    try {
      const contractsFind = await Contract.find();
      return res.send(contractsFind);
    } catch (error) {
      return res.status(500).send({
        message: 'Algum erro interno ocorreu. Tente novamente mais tarde',
        error,
      });
    }
  }

  async store(req, res) {
    const userToken = res.locals.decryptedToken;
    if (userToken) {
      utils.validateValues(req.body).then(() => {
        if (!req.body.criadoEm) {
          req.body.criadoEm = new Date()
        }

        if (req.body.assCPF_CNPJ.includes('-') || req.body.assCPF_CNPJ.includes('.') || req.body.assCPF_CNPJ.includes('/')) {
          req.body.assCPF_CNPJ = req.body.assCPF_CNPJ.replace(/-/g, '').replace(/\./g, '').replace(/\//g, '');
        }

        if (req.body.dataCoobrastur) {
          const dataCoobrastur = req.body.dataCoobrastur.toString();
          const dia = dataCoobrastur.substring(0, 2);
          const mes = dataCoobrastur.substring(3, 5);
          const ano = dataCoobrastur.substring(6, 10)
          req.body.dataCoobrastur = format(new Date(`${ano}-${mes}-${dia}`), 'yyyy-MM-dd')
        }

        if (req.body.dataVendedor) {
          const dataVendedor = req.body.dataVendedor.toString();
          const dia = dataVendedor.substring(0, 2);
          const mes = dataVendedor.substring(3, 5);
          const ano = dataVendedor.substring(6, 10)
          req.body.dataVendedor = format(new Date(`${ano}-${mes}-${dia}`), 'yyyy-MM-dd')
        }

        if (req.body.dataProgramada) {
          const dataProgramada = req.body.dataProgramada.toString();
          const dia = dataProgramada.substring(0, 2);
          const mes = dataProgramada.substring(3, 5);
          const ano = dataProgramada.substring(6, 10)
          /* if (!dia || !mes || !ano) {
            res.status(400).send({ message: 'Data programada inválida' })
          } */
          req.body.dataProgramada = format(new Date(`${ano}-${mes}-${dia}`), 'yyyy-MM-dd')
        }

        if (req.body.rascunho === undefined || req.body.rascunho === null) {
          req.body.rascunho = 0;
        }

        global.sql.request()
          .input('in_token', userToken)
          .input('in_rascunho', req.body.rascunho)
          .input('in_criadoEm', req.body.criadoEm)
          .input('in_assCPF_CNPJ', req.body.assCPF_CNPJ)
          .input('in_assNome_RazaoSocial', req.body.assNome_RazaoSocial)
          .input('in_endUF', req.body.endUF)
          .input('in_assEmailPessoal', req.body.assEmailPessoal)
          .input('in_assNumCelularDDD', req.body.assNumCelularDDD)
          .input('in_assNumCelular', req.body.assNumCelular)
          .input('in_indicacao', req.body.indicacao)
          .input('in_nomeIndicador', req.body.nomeIndicador)
          .input('in_cpfIndicador', req.body.cpfIndicador)
          .input('in_emailIndicador', req.body.emailIndicador)
          .input('in_dddCelularIndicador', req.body.dddCelularIndicador)
          .input('in_celularIndicador', req.body.celularIndicador)
          .input('in_tipoVenda', req.body.tipoVenda)
          .input('in_tpplcodigo', req.body.tpplcodigo)
          .input('in_plQtdeDiarias', req.body.plQtdeDiarias)
          .input('in_plfamilia', req.body.plfamilia)
          .input('in_adesao', req.body.adesao)
          .input('in_valorAdesao', req.body.valorAdesao)
          .input('in_meioAdesao', req.body.meioAdesao)
          .input('in_tipoAdesaoVendedor', req.body.tipoAdesaoVendedor)
          .input('in_formaAdesaoVendedor', req.body.formaAdesaoVendedor)
          .input('in_dataVendedor', req.body.dataVendedor)
          .input('in_obsVendedor', req.body.obsVendedor)
          .input('in_tipoAdesaoCoobrastur', req.body.tipoAdesaoCoobrastur)
          .input('in_formaAdesaoCoobrastur', req.body.formaAdesaoCoobrastur)
          .input('in_dataCoobrastur', req.body.dataCoobrastur)
          .input('in_obsCoobrastur', req.body.obsCoobrastur)
          .input('in_fiador', req.body.fiador)
          .input('in_fiaCpf', req.body.fiaCpf)
          .input('in_fianome', req.body.fianome)
          .input('in_fiaDtNascimento', req.body.fiaDtNascimento)
          .input('in_fianacionalidade', req.body.fianacionalidade)
          .input('in_civCodigo', req.body.civCodigo)
          .input('in_fiaConjuge', req.body.fiaConjuge)
          .input('in_fiaConjugeFiliacao', req.body.fiaConjugeFiliacao)
          .input('in_fiaConjugeFiliacao2', req.body.fiaConjugeFiliacao2)
          .input('in_fiaEmailPessoal', req.body.fiaEmailPessoal)
          .input('in_fiaNumeroCelularDDD', req.body.fiaNumeroCelularDDD)
          .input('in_fiaNumeroCelular', req.body.fiaNumeroCelular)
          .input('in_fiaddFone', req.body.fiaddFone)
          .input('in_fiaFone', req.body.fiaFone)
          .input('in_formaAssinaturaFiador', req.body.formaAssinaturaFiador)
          .input('in_assinaturaFiador', req.body.assinaturaFiador)
          .input('in_terceiro', req.body.terceiro)
          .input('in_cpfTerceiro', req.body.cpfTerceiro)
          .input('in_nomeTerceiro', req.body.nomeTerceiro)
          .input('in_nascimentoTerceiro', req.body.nascimentoTerceiro)
          .input('in_nacionalidadeTerceiro', req.body.nacionalidadeTerceiro)
          .input('in_estadoCivilTerceiro', req.body.estadoCivilTerceiro)
          .input('in_nomeConjugeTerceiro', req.body.nomeConjugeTerceiro)
          .input('in_nomeMaeTerceiro', req.body.nomeMaeTerceiro)
          .input('in_nomePaiTerceiro', req.body.nomePaiTerceiro)
          .input('in_emailTerceiro', req.body.emailTerceiro)
          .input('in_dddCelularTerceiro', req.body.dddCelularTerceiro)
          .input('in_numCelularTerceiro', req.body.numCelularTerceiro)
          .input('in_dddTelefoneTerceiro', req.body.dddTelefoneTerceiro)
          .input('in_TelefoneTerceiro', req.body.TelefoneTerceiro)
          .input('in_formaAssinaturaTerceiro', req.body.formaAssinaturaTerceiro)
          .input('in_assinaturaTerceiro', req.body.assinaturaTerceiro)
          .input('in_formaAssinatura', req.body.formaAssinatura)
          .input('in_assinatura', req.body.assinatura)
          .input('in_assRestricao', req.body.assRestricao)
          //.input('in_vendCodigo', req.body.vendCodigo)
          .input('in_aditamento', req.body.aditamento)
          .input('in_formaAditamento', req.body.formaAditamento || req.body.formaaditamento)
          .input('in_dataProgramada', req.body.dataProgramada)
          .input('in_endCepAssociado', req.body.endCepAssociado)
          .input('in_endCidadeAssociado', req.body.endCidadeAssociado)
          .input('in_endBairroAssociado', req.body.endBairroAssociado)
          .input('in_endLogradouroAssociado', req.body.endLogradouroAssociado)
          .input('in_endNumLogradouroAssociado', req.body.endNumLogradouroAssociado)
          .input('in_endComplementoAssociado', req.body.endComplementoAssociado)
          .execute('APP_VENDA_INSERT')
          .then((result) => handleResult(result.recordsets))
          .catch(err => { res.status(403).json(sqlServerUtils.errorExtractor(err)) });


        const handleResult = (result) => {
          const returnInfo = result[0][0];
          const d4signInfo = result[1];
          if (returnInfo.tokenPagamento) {
            returnInfo.tokenPagamento = token.encrypt(returnInfo.tokenPagamento);
          }
          res.send(returnInfo);
          console.log(chalk.green(`[VENDA ${returnInfo.id}] Etapa 1 - Venda inserida no sistema`));
        }
      }).catch((error) => {
        res.status(400).send({ error: 1, message: error });
      })
    } else {
      res.sendStatus(400);
    }
  }

  async patchById(req, res) {
    const userToken = res.locals.decryptedToken;
    if (userToken) {
      utils.validateValues(req.body).then(() => {
        if (!req.body.criadoEm) {
          req.body.criadoEm = new Date()
        }

        if (req.body.assCPF_CNPJ.includes('-') || req.body.assCPF_CNPJ.includes('.') || req.body.assCPF_CNPJ.includes('/')) {
          req.body.assCPF_CNPJ = req.body.assCPF_CNPJ.replace(/-/g, '').replace(/\./g, '').replace(/\//g, '');
        }

        if (req.body.dataCoobrastur) {
          const dataCoobrastur = req.body.dataCoobrastur.toString();
          const dia = dataCoobrastur.substring(0, 2);
          const mes = dataCoobrastur.substring(3, 5);
          const ano = dataCoobrastur.substring(6, 10)
          req.body.dataCoobrastur = format(new Date(`${ano}-${mes}-${dia}`), 'yyyy-MM-dd')
        }

        if (req.body.dataVendedor) {
          const dataVendedor = req.body.dataVendedor.toString();
          const dia = dataVendedor.substring(0, 2);
          const mes = dataVendedor.substring(3, 5);
          const ano = dataVendedor.substring(6, 10)
          req.body.dataVendedor = format(new Date(`${ano}-${mes}-${dia}`), 'yyyy-MM-dd')
        }

        if (req.body.dataProgramada) {
          const dataProgramada = req.body.dataProgramada.toString();
          const dia = dataProgramada.substring(0, 2);
          const mes = dataProgramada.substring(3, 5);
          const ano = dataProgramada.substring(6, 10)
          /* if (!dia || !mes || !ano) {
            res.status(400).send({ message: 'Data programada inválida' })
          } */
          req.body.dataProgramada = format(new Date(`${ano}-${mes}-${dia}`), 'yyyy-MM-dd')
        }

        if (req.body.rascunho === undefined || req.body.rascunho === null) {
          req.body.rascunho = 0;
        }

        global.sql.request()
          .input('in_token', userToken)
          .input('in_appvenda', req.body.id)
          .input('in_rascunho', req.body.rascunho)
          .input('in_criadoEm', req.body.criadoEm)
          .input('in_assCPF_CNPJ', req.body.assCPF_CNPJ)
          .input('in_assNome_RazaoSocial', req.body.assNome_RazaoSocial)
          .input('in_endUF', req.body.endUF)
          .input('in_assEmailPessoal', req.body.assEmailPessoal)
          .input('in_assNumCelularDDD', req.body.assNumCelularDDD)
          .input('in_assNumCelular', req.body.assNumCelular)
          .input('in_indicacao', req.body.indicacao)
          .input('in_nomeIndicador', req.body.nomeIndicador)
          .input('in_cpfIndicador', req.body.cpfIndicador)
          .input('in_emailIndicador', req.body.emailIndicador)
          .input('in_dddCelularIndicador', req.body.dddCelularIndicador)
          .input('in_celularIndicador', req.body.celularIndicador)
          .input('in_tipoVenda', req.body.tipoVenda)
          .input('in_tpplcodigo', req.body.tpplcodigo)
          .input('in_plQtdeDiarias', req.body.plQtdeDiarias)
          .input('in_plfamilia', req.body.plfamilia)
          .input('in_adesao', req.body.adesao)
          .input('in_valorAdesao', req.body.valorAdesao)
          .input('in_meioAdesao', req.body.meioAdesao)
          .input('in_tipoAdesaoVendedor', req.body.tipoAdesaoVendedor)
          .input('in_formaAdesaoVendedor', req.body.formaAdesaoVendedor)
          .input('in_dataVendedor', req.body.dataVendedor)
          .input('in_obsVendedor', req.body.obsVendedor)
          .input('in_tipoAdesaoCoobrastur', req.body.tipoAdesaoCoobrastur)
          .input('in_formaAdesaoCoobrastur', req.body.formaAdesaoCoobrastur)
          .input('in_dataCoobrastur', req.body.dataCoobrastur)
          .input('in_obsCoobrastur', req.body.obsCoobrastur)
          .input('in_fiador', req.body.fiador)
          .input('in_fiaCpf', req.body.fiaCpf)
          .input('in_fianome', req.body.fianome)
          .input('in_fiaDtNascimento', req.body.fiaDtNascimento)
          .input('in_fianacionalidade', req.body.fianacionalidade)
          .input('in_civCodigo', req.body.civCodigo)
          .input('in_fiaConjuge', req.body.fiaConjuge)
          .input('in_fiaConjugeFiliacao', req.body.fiaConjugeFiliacao)
          .input('in_fiaConjugeFiliacao2', req.body.fiaConjugeFiliacao2)
          .input('in_fiaEmailPessoal', req.body.fiaEmailPessoal)
          .input('in_fiaNumeroCelularDDD', req.body.fiaNumeroCelularDDD)
          .input('in_fiaNumeroCelular', req.body.fiaNumeroCelular)
          .input('in_fiaddFone', req.body.fiaddFone)
          .input('in_fiaFone', req.body.fiaFone)
          .input('in_formaAssinaturaFiador', req.body.formaAssinaturaFiador)
          .input('in_assinaturaFiador', req.body.assinaturaFiador)
          .input('in_terceiro', req.body.terceiro)
          .input('in_cpfTerceiro', req.body.cpfTerceiro)
          .input('in_nomeTerceiro', req.body.nomeTerceiro)
          .input('in_nascimentoTerceiro', req.body.nascimentoTerceiro)
          .input('in_nacionalidadeTerceiro', req.body.nacionalidadeTerceiro)
          .input('in_estadoCivilTerceiro', req.body.estadoCivilTerceiro)
          .input('in_nomeConjugeTerceiro', req.body.nomeConjugeTerceiro)
          .input('in_nomeMaeTerceiro', req.body.nomeMaeTerceiro)
          .input('in_nomePaiTerceiro', req.body.nomePaiTerceiro)
          .input('in_emailTerceiro', req.body.emailTerceiro)
          .input('in_dddCelularTerceiro', req.body.dddCelularTerceiro)
          .input('in_numCelularTerceiro', req.body.numCelularTerceiro)
          .input('in_dddTelefoneTerceiro', req.body.dddTelefoneTerceiro)
          .input('in_TelefoneTerceiro', req.body.TelefoneTerceiro)
          .input('in_formaAssinaturaTerceiro', req.body.formaAssinaturaTerceiro)
          .input('in_assinaturaTerceiro', req.body.assinaturaTerceiro)
          .input('in_formaAssinatura', req.body.formaAssinatura)
          .input('in_assinatura', req.body.assinatura)
          .input('in_assRestricao', req.body.assRestricao)
          //.input('in_vendCodigo', req.body.vendCodigo)
          .input('in_aditamento', req.body.aditamento)
          .input('in_formaAditamento', req.body.formaAditamento || req.body.formaaditamento)
          .input('in_dataProgramada', req.body.dataProgramada)
          .input('in_endCepAssociado', req.body.endCepAssociado)
          .input('in_endCidadeAssociado', req.body.endCidadeAssociado)
          .input('in_endBairroAssociado', req.body.endBairroAssociado)
          .input('in_endLogradouroAssociado', req.body.endLogradouroAssociado)
          .input('in_endNumLogradouroAssociado', req.body.endNumLogradouroAssociado)
          .input('in_endComplementoAssociado', req.body.endComplementoAssociado)
          .execute('APP_VENDA_UPDATE')
          .then((result) => handleResult(result.recordsets))
          .catch(err => { res.status(403).json(sqlServerUtils.errorExtractor(err)) });


        const handleResult = (result) => {
          const returnInfo = result[0][0];
          const d4signInfo = result[1];
          if (returnInfo.tokenPagamento) {
            returnInfo.tokenPagamento = token.encrypt(returnInfo.tokenPagamento);
          }
          res.send(returnInfo);
          console.log(chalk.green(`[VENDA ${returnInfo.id}] Etapa 1 - Venda inserida no sistema`));
        }
      }).catch((error) => {
        res.status(400).send({ error: 1, message: error });
      })
    } else {
      res.sendStatus(400);
    }
  }

  async insertPaymentCredit(req, res) {


    global.sql.request()
      .input('in_tokenPagamento', token.decrypt(req.body.tokenPagamento))
      //.input('in_appVenda', req.body.appVenda)
      .input('in_formaPagamento', 2)
      .input('in_cpfTitularCartao', req.body.cpfTitularCartao)
      .input('in_nomeTitularCartao', req.body.nomeTitularCartao)
      .input('in_carNumero', req.body.carNumero)
      .input('in_carValidade', req.body.carValidade)
      .input('in_banCodigo', req.body.banCodigo)
      .input('in_carcodigo_seguranca', req.body.carcodigo_seguranca)
      .input('in_carDia', req.body.carDia)
      .execute('APP_VENDA_INSERT_DADOS_PAGAMENTO')
      .then((result) => handleResult(result.recordset))
      .catch(err => { res.status(403).json(sqlServerUtils.errorExtractor(err)) });
    const handleResult = async (result) => {
      res.sendStatus(200);
      const contract = result[0];
      let envioAssinatura;
      console.log(chalk.green(`[VENDA ${contract.id}] Etapa 2 - Pagamento inserido`));
      try {
        envioAssinatura = await enviarVenda(contract);
        const uuid = envioAssinatura?.uuid;
        await setD4SignStatus({ appVenda: contract.id, uuid });
        console.log(chalk.green(`[VENDA ${contract.id}] Etapa 3 - Envio para D4SIGN`));
      } catch (err) {
        console.log(chalk.red(`[VENDA ${contract.id}] Etapa 3 - Erro no envio para D4SIGN: ${err}`));
      }
    }

  }





  async insertPaymentDebit(req, res) {
    global.sql.request()
      .input('in_tokenPagamento', token.decrypt(req.body.tokenPagamento))
      .input('in_formaPagamento', 1)
      .input('in_cpfTitularContaCorrente', req.body.cpfTitularContaCorrente)
      .input('in_nomeTitularContaCorrente', req.body.nomeTitularContaCorrente)
      .input('in_bcoCodigo', req.body.bcoCodigo)
      .input('in_conContaCorrente', req.body.conContaCorrente)
      .input('in_conContaCorrenteDV', req.body.conContaCorrenteDV)
      .input('in_conAgencia', req.body.conAgencia)
      .input('in_conAgenciaDV', req.body.conAgenciaDV)
      .input('in_debDia', req.body.debDia)
      .execute('APP_VENDA_INSERT_DADOS_PAGAMENTO')
      .then((result) => handleResult(result.recordset))
      .catch(err => { res.status(403).json(sqlServerUtils.errorExtractor(err)) });

    const handleResult = async (result) => {
      console.log(result);
      res.sendStatus(200);
      const contract = result[0];
      let envioAssinatura;
      console.log(chalk.green(`[VENDA ${contract.id}] Etapa 2 - Pagamento inserido`));

      try {
        envioAssinatura = await enviarVenda(contract);
        const uuid = envioAssinatura?.uuid;
        await setD4SignStatus({ appVenda: contract.id, uuid });
        console.log(chalk.green(`[VENDA ${contract.id}] Etapa 3 - Envio para D4SIGN`));
      } catch (err) {
        console.log(chalk.red(`[VENDA ${contract.id}] Etapa 3 - Erro no envio para D4SIGN: ${err}`));
      }
    }
  }

  async all(req, res) {
    const token = res.locals.decryptedToken;
    if (token) {
      global.sql.request()
        .input('in_token', token)
        .execute('APP_VENDA_SELECT_ALL')
        .then((result) => res.send(result.recordset))
        .catch(err => { res.status(403).json(err); });
    } else {
      res.sendStatus(400);
    }
  }

  async update(req, res) {
    const { id } = req.params;
    const newContract = req.body;

    if (!id)
      return res.send({ message: 'Contrato não existe na base de dados' });

    try {
      const options = { new: true };
      await Contract.findByIdAndUpdate(id, newContract, options);

      return res.send({ message: 'Contrato atualizado com sucesso' });
    } catch (error) {
      return res.status(500).send({
        message: 'Algum erro interno ocorreu. Tente novamente mais tarde',
      });
    }
  }

  async delete(req, res) {
    const { id } = req.params;
    if (!id) return res.send({ delete: false, id: null });

    try {
      const contractFind = await Contract.findByIdAndRemove(id);
      if (!contractFind) return res.send({ message: 'Contrato não existente' });

      return res.send({ message: 'Contrato deletado com sucesso' });
    } catch (error) {
      return res
        .status(500)
        .send({ message: 'Não foi possível deletar o contrato', error });
    }
  }
}

export default new ContractController();
