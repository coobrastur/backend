# Nova API do site de vendas da Coobrastur

  

Um novo modo de efetuar vendas com a Coobrastur

  

### Instalação

Para que você possa rodar o projeto em sua máquina é necessário ter algumas coisas instaladas:

- NodeJS

- Npm ou Yarn
  
### Executando o projeto

- Basta clonar este repositório em seu PC

- Execute `yarn` ou `npm install`

- E agora é só rodar o projeto com um dos comandos a seguir: `yarn run dev OU npm run dev`

### Desenvolver

É necessário ter Node.JS ou Docker instalado para rodar a API, e para auxiliar o desenvolvimento foi disposto um servidor com Linux para nós, que pode ser acessado via SSH internamente na Coobrastur pelo ip 172.16.37.16, usuário "desenv" e senha "D%v461721".

Sugiro instalar a extensão "Remote - SSH" do VS Code para desenvolver na máquina remota de uma forma igualmente cômoda a uma máquina local.

As portas acessíveis na máquina virtual são 5005 e 5000. Se for trabalhar em conjunto de outra pessoa, pode ser disposta uma porta para testes do frontend e outra porta para desenvolvimento do backend.
 
 O arquivo .env que está presente nesta máquina está com todas as variáveis de ambiente necessárias.

Qualquer dúvida que precisar para conectar nessa máquina remota, falar com o Olmiro.

### Build

O projeto tem um Dockerfile com as configurações necessárias e pronto para gerar um container. Sugiro seguir este tutorial para mais informações de quando for preciso gerar o container. https://buddy.works/guides/how-dockerize-node-application

Atenção: As únicas portas que podem ser acessadas de fora da máquina são 5000 e 5005, não usar 8081 conforme o tutorial.

### SQL - Tabelas e procedures  

Todas as tabelas e procedures referentes a API tem o prefixo "APP_VENDA" e estão armazenadas (inicialmente para testes) no banco do Triton por ter o ambiente configurado e o sistema de login que já é usado no app de vendas offline. (Endereço do banco: 10.1.2.174, banco bdCRM).

- Tabelas:
	- **APP_VENDAS** -> Armazena vendas e todas as informações necessárias para o envio posterior para o Siscoob

	- **APP_VENDAS_ALTERACOES_PAGAMENTOS** -> Armazena ordens de alteração de pagamentos que são criadas assim que uma linha é criada na APP_VENDAS. Para o cliente conseguir enviar os dados de cartão, tem que ter uma 

	- **APP_VENDAS_D4SIGN** -> Armazena o UUID da D4SIGN, data de criação do contrato na D4SIGN e a data em que o mesmo foi assinado. O STATUS segue o padrão da D4SIGN, 1 = assinado.

- Procedures
	- **APP_VENDA_INSERT** -> Ao ser rodada com sucesso, insere vendas na tabela APP_VENDAS e APP_VENDAS_ALTERACOES_PAGAMENTOS

	- **APP_VENDA_SELECT_BY_TOKEN** -> Seleciona todas as vendas do usuário do token inserido.

	- **APP_VENDA_SELECT_ALL** -> Seleciona todas as vendas que o usuário tem acesso (adm empresa tem acesso a todas da empresa, adm geral tem acesso a todas).
 
	- **APP_VENDA_DELETE_BY_ID** -> Deleta venda por ID, se o usuário tiver permissão disso.

	- **APP_VENDA_INSERT_DADOS_PAGAMENTO** -> Invalida um token de pagamento da tabela *APP_VENDAS_ALTERACOES_PAGAMENTOS* para inserir dados de pagamento em uma venda.
 
	- **APP_VENDA_DELETE_BY_ID** -> Deleta venda por ID, se o usuário tiver permissão disso.

	- **APP_VENDA_INSERT_DADOS_D4SIGN** -> Altera o status na tabela *APP_VENDAS_D4SIGN*. Usado para informar se a venda já foi enviada, inserir UUID gerado pela d4sign na venda.

	- **APP_VENDA_SELECT_D4SIGN_NAO_ENVIADOS** -> Obtém vendas não enviadas pela D4SIGN para tentar enviar novamente.
	
	-  **APP_VENDA_SELECT_TO_D4SIGN** -> Obtém os dados de uma venda que são enviados para a D4SIGN.

	- **APP_VENDA_SELECT_USUARIO** -> Sem endpoint criado, mas procedure seleciona os usuários da empresa do usuário adm de empresa ou adm geral.

	- **APP_VENDA_UPDATE** -> Atualiza dados de uma venda.

### Fluxo de venda normal e status do projeto.
- **Passo 1**: Recebimento e validação de vendas ✅
- **Passo 2**: Geração de token de pagamento para posterior envio ✅
	- **Passo 2.1**: Envio de e-mail com token na tabela APP_VENDAS_ALTERACOES_PAGAMENTO ❌
- **Passo 3**: Envio para a D4sign ✅
	- **Passo 3.1**: Recebimento de webhook da D4sign❗️(Feito, mas não é possível testar pois API não é acessível de fora da Coobrastur.
- **Passo 4**: Envio para o Siscoob ❌

### Extensão útil
Sugiro instalar a extensão Todo Tree que mostrará partes do código que estão marcadas como "TODO" (a fazer)

https://marketplace.visualstudio.com/items?itemName=Gruntfuggly.todo-tree
