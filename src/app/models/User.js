import mongoose from 'mongoose';
import bcrypt from 'bcryptjs';

const schema = new mongoose.Schema({
  user: { type: String, require: true, unique: true },
  password: { type: String, require: true, select: false },
  cpf: { type: String },
  name: { type: String },
  email: { type: String },
  access: { type: Number },
  status: { type: Number, default: 1 },
  createdAt: { type: Date, default: new Date() },
  updatedAt: { type: Date, default: new Date() },
});

schema.pre('save', async function (next) {
  this.password = await bcrypt.hash(this.password, 8);
  return next();
});

module.exports = mongoose.model('User', schema);
