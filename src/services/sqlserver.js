
import sql from "mssql";

const CRM_IP = process.env.NODE_ENV === 'production' ? process.env.CRM_IP_PROD : process.env.CRM_IP_DEV;
const CRM_USER = process.env.CRM_USER;
const CRM_PASSWORD = process.env.CRM_PASSWORD;
const CRM_DB = process.env.CRM_DATABASE;

let dbConfig = {
  user: CRM_USER,
  password: CRM_PASSWORD,
  server: CRM_IP,
  database: CRM_DB,
  options: {
    encrypt: false,
    enableArithAbort: true
  },
};

sql.connect(dbConfig)
  .then(conn => { global.sql = conn; console.log("SUCCESS: SqlServer") })
  .catch(err => console.log("Erro na conexão do SQL Server! " + err));



class sqlServerUtils {

  errorExtractor = (err) => {
    const error = err.originalError?.info?.class + '-' + err.originalError?.info?.state + '/' + err.originalError?.info?.lineNumber;
    const message = err.originalError?.info?.message;
    if (error && message) {
      return ({ error, message });
    } else {
      if (process.NODE_ENV === 'development') {
        return ({ error: '1', message: 'Erro desconhecido', developmentDebug: err });
      }

      return ({ error: '1', message: 'Erro desconhecido' });


    }
  }

}

export default new sqlServerUtils();
