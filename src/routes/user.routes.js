import { Router } from 'express';
import UserController from '../app/controllers/UserController';

const userRoutes = Router();

/**
* @swagger
* /user/login:
*   post:
*     summary: Login no sistema.
*     description: Utiliza o sistema de autenticação do Triton para criar sessão e token.
*     tags:
*      - user
*     requestBody:
*       content:
*         application/json:
*           schema:
*             type: object
*             properties:
*               username:
*                 type: string
*               password:
*                 type: string
*               ipAcesso:
*                 type: string
*     responses:
*       200:
*         description: Autenticação feita com sucesso, sessão criada e token retornado.
*         content:
*           application/json:
*             schema:
*               type: array
*               items:
*                 $ref: '#/definitions/user-minified'
*       403:
*         description: Não autorizado/Usuário incorreto/Usuário Não habilitado.
*       400:
*         description: Bad request
*/
userRoutes.post('/login', UserController.login);

/**
* @swagger
* /user/me:
*   get:
*     summary: Obtém informações do usuário.
*     description: Obtém todas as informações disponíveis do usuário no sistema.
*     tags:
*      - user
*     responses:
*       200:
*         description: TODO.
*         content:
*           application/json:
*             schema:
*               type: array
*               items:
*                 $ref: '#/components/schemas/user-minified'
*       403:
*         description: Não autorizado/Usuário incorreto/Usuário Não habilitado.
*       400:
*         description: Bad request
*/
userRoutes.get('/me', UserController.me);

export default userRoutes;
