import { celebrate, Segments, Joi } from 'celebrate';

class ValidatorUser {
  constructor() {
    this.create = celebrate({
      [Segments.BODY]: Joi.object().keys({
        user: Joi.string().required(),
        password: Joi.string().required(),
        cpf: Joi.string().required(),
        name: Joi.string().required(),
        email: Joi.string().required().email(),
      }),
    });
  }
}

export default new ValidatorUser();
