import 'dotenv/config';
import express from 'express';
import cors from 'cors';
import { errors } from 'celebrate';
import routes from './src/routes';
//import './src/services/mongodb';
import './src/services/sqlserver';
import token from './src/services/token'
import swaggerJsDoc from "swagger-jsdoc";
import swaggerUi from "swagger-ui-express";
import chalk from 'chalk';
import figlet from 'figlet';
import 'express-async-errors';
import cron from 'node-cron';
import { verificarVendasNaoEnviadas } from './src/services/d4sign';

const app = express();
const port = process.env.PORT || 5000
app.use(cors());

if (process.env.NODE_ENV === 'production') {
  console.log(chalk.red(
    figlet.textSync('api-sales-Coobrastur', { horizontalLayout: 'full' })
  ))
  console.log(chalk.red(`***** AMBIENTE DE PRODUÇÃO *****`))
} else {
  const options = require('./swagger.json');
  const swaggerDocs = swaggerJsDoc(options);
  app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));

  // Limpar console
  process.stdout.write("\u001b[2J\u001b[0;0H");

  console.log(chalk.blue(
    figlet.textSync('api-sales-Coobrastur', { horizontalLayout: 'full' })
  ))
  console.log(chalk.blue(`Ambiente de desenvolvimento. Documentação disponível em`), chalk.yellow(`http://localhost:${port}/api-docs`))
}

app.use(async function (req, res, next) {
  const unsafeRoutes = [
    '/user/login',
    '/api-docs/',
    '/contratos/insert-payment/credit',
    '/contratos/insert-payment/debit'
  ]
  if (unsafeRoutes.includes(req.path)) {
    return next()
  }

  const authHeader = req.headers['authorization']
  if (!authHeader) return res.sendStatus(401)
  const decryptedToken = await token.check(authHeader.split(' ')[1]);
  if (token) {
    res.locals.decryptedToken = decryptedToken;
    next();
  } else {
    res.sendStatus(403);
  }
});

app.use((err, req, res, next) => {
  // handle error here
  console.log('handler', err)
  if (err) {
    res.send(err)
  }

});


app.use(express.json());
app.use(routes);
app.use(errors());

app.listen(port, () => {
  console.log('Tudo está funcionando corretamente✅');
});

/**
 * CRON TEST
 # ┌────────────── second (optional)
 # │ ┌──────────── minute
 # │ │ ┌────────── hour
 # │ │ │ ┌──────── day of month
 # │ │ │ │ ┌────── month
 # │ │ │ │ │ ┌──── day of week
 # │ │ │ │ │ │
 # │ │ │ │ │ │
 # * * * * * *
 */

cron.schedule('*/20 * * * *', () => {
  verificarVendasNaoEnviadas();
});
