import { celebrate, Segments, Joi } from 'celebrate';

class ValidatorProjects {
  constructor() {
    this.create = celebrate({
      [Segments.BODY]: Joi.object().keys({
        cpf: Joi.string().required(),
        name: Joi.string().required(),
        email: Joi.string ().required(),
        status: Joi.number().required(),
      }),
    });
  }
}

export default new ValidatorProjects();
