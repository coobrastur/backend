import crypto from 'crypto';
import sql from 'mssql';
import base64url from 'base64url';

const alg = 'aes-256-cbc';
const CRYPTO_KEY = process.env.CRYPTO_KEY;

class token {

  check = (token) => {
    return new Promise((resolve, reject) => {
      token = this.decrypt(token);
      if (token.length === 36) {
        global.sql.request()
          .input('token', sql.UniqueIdentifier, token)
          .query('select top 1 1 from tokens where token = @token')
          .then((result) => {
            if (result.recordset.length > 0) {
              resolve(token)
            }
          })
          .catch(err => { reject() });
      } else {
        reject();
      }
    })
  }

  encrypt = (text) => {
    return base64url(text);

    /**
     * Criptografia tá funcionando normalmente, mas como não traz mais segurança criptografar o token, não está sendo usada.
     * Talvez possa auxiliar com o armazenamento do cartão de crédito, então mantendo a função comentada.
     */
    /* const iv = crypto.randomBytes(16);
    const cipher = crypto.createCipheriv(
      alg, Buffer.from(CRYPTO_KEY), iv);
    let encrypted = cipher.update(text);
    encrypted = Buffer.concat([encrypted, cipher.final()]);
    const finalString = iv.toString('hex') + ':' + encrypted.toString('hex');
    return Buffer.from(finalString).toString('base64url'); */
  }

  decrypt = (text) => {


    return base64url.decode(text);
    /**
     * DECRYPTION WORKING
     */
    /* text = Buffer.from(text, 'base64url').toString('ascii');
    const textParts = text.split(':');
    if (textParts.length === 2) {
      const iv = Buffer.from(textParts.shift(), 'hex');
      const encryptedText = Buffer.from(textParts[0], 'hex');
      const decipher = crypto.createDecipheriv(alg, Buffer.from(CRYPTO_KEY), iv);
      let decrypted = decipher.update(encryptedText);
      decrypted = Buffer.concat([decrypted, decipher.final()]);
      return decrypted.toString();
    } else {
      return undefined;
    } */

  }
}

export default new token();
