import axios from 'axios'
import chalk from 'chalk';

import {
  restricaoArray,
  booleanArray,
  ufArray,
  tipoVendaArray,
  planoArray,
  planoDescricaoArray,
  diariasArray,
  diariasGoArray,
  bandeiraCartaoArray,
  formaPagamentoMensalidadeArray,
  diaMensalidadeBancoArray,
  diaMensalidadeCartaoArray,
  bancoContaArray,
  bancoCaixaTipoArray,
  estadoCivilArray,
  formaAssinaturaArray,
  adesaoMeioArray,
  formaPagamentoAdesaoVendedorArray,
  formaPagamentoAdesaoCoobrasturArray,
  parcelasPagamentoAdesaoArray,
  formaAditamentoArray,
  valorMensalidadePlanoFamiliaArray,
  valorMensalidadePlanoArray,
} from '../app/middlewares/datas'

import {
  format,
  formatReal,
  labelByValue,
  getMensalidade
} from '../app/middlewares/utils';


const INITIAL_TOKEN = process.env.INITIAL_TOKEN_D4SIGN_PROD;
const TOKEN_API = process.env.TOKEN_D4SIGN_PROD;
const CRYPT_KEY =process.env.CRYPT_KEY_D4SIGN_PROD;


const apiD4sign = axios.create({
  baseURL:"https://secure.d4sign.com.br/api/v1/",
  headers: {
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Headers": "*",
    "Access-Control-Allow-Methods": "*",

    "access-control-allow-origin": "*",
    "access-control-allow-headers": "*",
    "access-control-allow-methods": "*",

    "Content-Type": "application/json",
    "content-type": "application/json",
  },
});


function setD4SignStatus({ appVenda, uuid, status }) {
  return new Promise((resolve, reject) => {
    const d4signStatus = global.sql.request().input('in_appVenda', appVenda);

    if (uuid) {
      d4signStatus.input('in_uuid', uuid)
    }

    if (status) {
      d4signStatus.input('in_status', status)
    }

    d4signStatus
      .execute('APP_VENDA_INSERT_DADOS_D4SIGN')
      .then((result) => { resolve(result) })
      .catch(err => { reject(err) });
  })

}

function verificarVendasNaoEnviadas() {
  global.sql.request()
    .execute('APP_VENDA_SELECT_D4SIGN_NAO_ENVIADOS')
    .then(async (result) => {
      if (result.recordset.length) {
        console.log('Vendas não enviadas');
        const vendas = result.recordset;
        for (const venda of vendas) {
          try {
            const infoVenda = await obterInformacaoVenda(venda.id);
            const res = await enviarVenda(infoVenda);
            await setD4SignStatus({ appVenda: venda.id, uuid: res.uuid });
            console.log(chalk.green(`[VENDA ${venda.id}] Etapa 3 - Reenvio para D4SIGN`));
          } catch (err) {
            console.log(chalk.red(`[VENDA ${venda.id}] - Etapa 3 - Erro no reenvio para a D4Sign: ${err}`));
          }
        }
      } else {
        console.log('Todas as vendas sincronizadas com a D4SIGN');
      }
    })
    .catch(err => console.log(err));

}

function obterInformacaoVenda(id) {
  return new Promise((resolve, reject) => {
    global.sql.request()
      .input('in_appvenda', id)
      .execute('APP_VENDA_SELECT_TO_D4SIGN')
      .then((result) => resolve(result.recordset[0]))
      .catch(err => reject(err));
  })
}

async function enviarVenda(contract) {
  return new Promise(async (resolve, reject) => {
    const d4SignDocument = await obterDocumentoD4sign(contract);
    const d4SignSigners = obterAssinantesD4sign(contract);
    const emailVendedor = contract.usuEmail;
    const usuarioVendedor = contract.usuUsuario;

    const contratoD4 = {
      documento: d4SignDocument,
      vendedor: {
        usuario: usuarioVendedor || null,
        email: emailVendedor || null,
      },
      signatarios: d4SignSigners
    }

    //D4SIGN NEW 

    //const { documento, signatarios, vendedor } = req.body;
    const { documento, signatarios, vendedor } = contratoD4;

    if (!documento) reject({ documento: null })
    if (!signatarios) reject({ signatarios: null })

    /* console.log("vendedor==>", JSON.stringify(vendedor), "<==vendedor"); */

    // 1) REGISTRO DOCUMENTO:
    let resCriacaoDocumento;
    try {
      resCriacaoDocumento = await apiD4sign.post(
        `documents/${INITIAL_TOKEN}/makedocumentbytemplate?tokenAPI=${TOKEN_API}&cryptKey=${CRYPT_KEY}`,
        documento
      );
    } catch (err) {
      reject('DOCUMENT_CREATE: ' + (err.response.data.mensagem_pt || err.response.data.message || err.response.data.error || 'Erro desconhecido ao criar documento'));
      return;
    }
    /* console.log(
      "resCriacaoDocumento.data==>",
      resCriacaoDocumento.data,
      "<==resCriacaoDocumento.data"
    ); */
    //  data: { message: 'success', uuid: '268998e0-23ba-4d93-b81b-96c56a32e094' }

    if (!resCriacaoDocumento.data)
      reject({ criacaoDocumento: null })

    if (!resCriacaoDocumento.data.uuid)
      reject({ uuid: null })

    const { uuid } = resCriacaoDocumento.data;

    // 2) REGISTRO SIGNATARIOS
    let resCriacaoSignatarios;
    try {
      resCriacaoSignatarios = await apiD4sign.post(
        `documents/${uuid}/createlist?tokenAPI=${TOKEN_API}&cryptKey=${CRYPT_KEY}`,
        { signers: signatarios }
      );
    } catch (err) {
      reject('SIGNERS: ' + err.response.data.mensagem_pt || err.response.data.message || err.response.data.error || 'Erro desconhecido ao enviar signatarios');
      return;
    }


    /* console.log(
      "resCriacaoSignatarios==>",
      resCriacaoSignatarios.data,
      "<==resCriacaoSignatarios"
    ); */

    if (!resCriacaoSignatarios.status === 200)
      reject({ criacaoSignatarios: false })

    // 3) ENVIO DOCUMENTO:
    let resEnvioDocumento;
    try {
      resEnvioDocumento = await apiD4sign.post(
        `documents/${uuid}/sendtosigner?tokenAPI=${TOKEN_API}&cryptKey=${CRYPT_KEY}`,
        {
          message: "Segue documento para assinatura enviado pelo Aplicativo",
          workflow: "0",
          skip_email: "0",
        }
      );
    } catch (err) {
      reject('DOCUMENT_SEND: ' + err.response.data.mensagem_pt || err.response.data.message || err.response.data.error || 'Erro desconhecido ao enviar documento para o assinante');
      return;
    }

    /* console.log(
      "resEnvioDocumento==>",
      resEnvioDocumento,
      "<==resEnvioDocumento"
    ); */

    if (!resEnvioDocumento.data)
      reject({ envioDocumento: null })

    if (!resEnvioDocumento.data.message)
      reject({ envioDocumento: false })


    //Devido a limitações na API da D4Sign, o cadastro de observadores funciona somente em produção.
    if (contratoD4.vendedor && contratoD4.vendedor.email !== undefined) {
      // 4) REGISTRA OBSERVADOR (VENDEDOR)
      let resVendedorObservador;
      try {
        resVendedorObservador = await apiD4sign.post(
          `watcher/${uuid}/add?tokenAPI=${TOKEN_API}&cryptKey=${CRYPT_KEY}`,
          {
            email: contratoD4.vendedor.email,
            permission: "1",
          }
        );
      } catch (err) {
        reject('OBSERVERS: ' + err.response.data.mensagem_pt || err.response.data.message || err.response.data.error || 'Erro desconhecido ao registrar observadores');
        return;
      }

      if (!resVendedorObservador.data)
        reject({
          uuid,
          criacaoDocumento: true,
          criacaoSignatarios: true,
          envioDocumento: true,
          envioVendedor: null,
        })

      /* console.log(
        "resVendedorObservador==>",
        resVendedorObservador.data,
        "<==resVendedorObservador"
      ); */

      if (!resVendedorObservador.data.message)
        reject({
          uuid,
          criacaoDocumento: true,
          criacaoSignatarios: true,
          envioDocumento: true,
          envioVendedor: false,
        })
    }

    let resWebhook;
    try {
      resWebhook = await apiD4sign.post(
        `documents/${uuid}/webhooks?tokenAPI=${TOKEN_API}&cryptKey=${CRYPT_KEY}`,
        {
          //TODO: Inserir URL que receberá postback da Coobrastur no endpoint já criado /d4sign/signed?uuid
          "url": `http://[URL_AQUI]/signed?${uuid}` 
        }
      );
    } catch (err) {
      reject('WEBHOOK: ' + err.response.data.mensagem_pt || err.response.data.message || err.response.data.error || 'Erro desconhecido ao registrar webhook');
      return;
    }

    if (!resWebhook.data.message)
      reject({ webhook: false })

    resolve({
      uuid,
      criacaoDocumento: true,
      criacaoSignatarios: true,
      envioDocumento: true,
      envioVendedor: true,
      webhook: true,
    })

  })
}

async function setStatusEnviado(id) {

}


async function obterDocumentoD4sign(contract) {

  const template = obterTemplateD4sign(contract);
  return {
    [String(
      'name_document',
    )]: `${contract.assCPF_CNPJ}/${contract.assNome_RazaoSocial}/${contract.usuNome}`,
    templates: {
      [template]: {
        //dados pessoais
        nomeAssociado: String(contract?.assNome_RazaoSocial),
        cpfAssociado: String(contract?.assCPF_CNPJ),

        //contato
        emailAssociado: String(contract?.assEmailPessoal),
        celularAssociado: String(
          `(${contract?.assNumCelularDDD}) ${contract?.assNumCelular}`,
        ),

        //associacao
        plano: String(labelByValue(contract?.tpplcodigo, planoArray)),
        diariasPlano: String(`${contract?.plQtdeDiarias} diárias`),
        familiaPlano: contract?.plfamilia ? 'Sim' : 'Não',
        valorMensalidade: String(
          formatReal(
            await getMensalidade({
              plano: contract?.tpplcodigo,
              diarias: contract?.plQtDeDiarias,
              familia: contract?.plfamilia || false,
            }),
          ),
        ),

        //TODO: Descobrir como obter isso: dataPrimeiraMensalidade. Existe propriedade contract.dataProgramada, seria isso?
        dataPrimeiraMensalidade: String('contract?.dataPrimeiraMensalidade'),
        // dataLiberacaoPlano: String(contract?.dataLiberacaoPlano),


        // ADESAO
        adesao: contract.adesao ? "Sim" : "Não",
        valorPagamentoAdesao: contract.adesao
          ? String(formatReal(contract.valorAdesao))
          : "Sem adesão",
        nomeCartao:
          contract.formaPagamento == 2 || contract.formaPagamento == 3
            ? String(contract?.nomeTitularCartao)
            : "",
        cpfCartao:
          contract.formaPagamento == 2 || contract.formaPagamento == 3
            ? String(contract?.cpfTitularCartao)
            : "",

        bandeiraCartao:
          contract.formaPagamento == 2 || contract.formaPagamento == 3
            ? String(
              labelByValue(contract.banCodigo, bandeiraCartaoArray)
            )
            : "",
        numeroCartao:
          contract.formaPagamento == 2 || contract.formaPagamento == 3 ? String(contract.carNumero) : "",
        ValidadeCartao:
          contract.formaPagamento == 2 || contract.formaPagamento == 3
            ? String(
              `${contract?.carValidade}`
            )
            : "",
        diaFaturaCartao:
          contract.formaPagamento == 2 || contract.formaPagamento == 3
            ? contract.adesao
              ? labelByValue(
                contract?.carDia,
                diaMensalidadeCartaoArray
              )
              : String(contract.carDia)
            : "",

        nomeConta:
          contract.formaPagamento == 1
            ? String(contract?.nomeTitularContaCorrente)
            : "",
        cpfConta:
          contract.formaPagamento == 1
            ? String(contract?.cpfTitularContaCorrente)
            : "",
        bancoConta:
          contract.formaPagamento == 1
            ? String(
              labelByValue(contract.bcoCodigo, bancoContaArray)
            )
            : "",
        numeroConta:
          contract.formaPagamento == 1
            ? contract.bcoCodigo == 104
              //TODO: Descobrir o que é isso: bancoCaixaTipo
              ? `${'contract.bancoCaixaTipo'}${contract.conContaCorrente}`
              : String(contract.conContaCorrente)
            : "",
        agenciaConta:
          contract.formaPagamento == 1
            ? String(
              contract.bcoCodigo == 41
                ? `${contract.conAgencia}-${contract.conAgenciaDV}`
                : contract.conAgencia
            )
            : "",
        dvConta:
          contract.formaPagamento == 1
            ? String(contract.conContaCorrenteDV)
            : "",
        diaDebitoConta:
          contract.formaPagamento == 1
            ? contract.adesao
              ? labelByValue(
                contract.debDia,
                diaMensalidadeBancoArray
              )
              : String(contract.debDia)
            : "",

        // RESTRICAO FIADOR
        fiador: String(contract?.fiador ? "Sim" : "Não"),
        nomeFiador: String(
          contract?.fiador ? contract?.fianome : "Sem fiador"
        ),
        cpfFiador: String(
          contract?.fiador ? contract?.fiaCpf : "Sem fiador"
        ),
        dataNascimentoFiador: String(
          contract.fiador
            ? masker(String(contract.fiaDtNascimento), "00/00/0000") || ""
            : "Sem fiador"
        ),
        estadoCivilFiador: String(
          contract?.fiador
            ? labelByValue(contract?.civCodigo, estadoCivilArray) ||
            ""
            : "Sem fiador"
        ),
        emailFiador: String(
          contract?.fiador ? contract?.fiaEmailPessoal || "" : "Sem fiador"
        ),
        celularFiador: String(
          contract?.fiador ? contract?.fiaNumeroCelularDDD + contract?.fiaNumeroCelular || "" : "Sem fiador"
        ),
        telefoneFiador: String(
          contract?.fiador ? contract?.fiaddFone + contract.fiaFone || "" : "Sem fiador"
        ),

        terceiro: contract.terceiro ? "Sim" : "Não",
        cpfTerceiro: contract.terceiro
          ? String(
            masker(String(contract.cpfTerceiro), "000.000.000-00") || "-"
          )
          : "-",
        nomeTerceiro: contract.terceiro
          ? String(contract.nomeTerceiro || "-")
          : "-",
        nascimentoTerceiro: contract.terceiro
          ? String(masker(contract.nascimentoTerceiro, "00/00/0000") || "-")
          : "-",
        nacionalidadeTerceiro: contract.terceiro
          ? String(contract.nacionalidadeTerceiro || "-")
          : "-",
        estadoCivilTerceiro: contract.terceiro
          ? String(contract.estadoCivilTerceiro || "-")
          : "-",
        nomeConjugeTerceiro: contract.terceiro
          ? String(contract.nomeConjugeTerceiro || "-")
          : "-",
        nomeMaeTerceiro: contract.terceiro
          ? String(contract.nomeMaeTerceiro || "-")
          : "-",
        nomePaiTerceiro: contract.terceiro
          ? String(contract.nomePaiTerceiro || "-")
          : "-",
        emailTerceiro: contract.terceiro
          ? String(contract.emailTerceiro || "-")
          : "-",
        celularTerceiro: contract.terceiro
          ? `(${contract.dddCelularTerceiro}) ${contract.numCelularTerceiro}` ||
          "-"
          : "-",
        telefoneTerceiro: contract.terceiro
          ? `(${contract.dddTelefoneTerceiro}) ${contract.TelefoneTerceiro}` ||
          "-"
          : "-",
        formaAssinaturaTerceiro: contract.terceiro
          ? String(
            contract.formaAssinaturaTerceiro == 1
              ? "Online (e-mail)"
              : "Offline (Dedo)" || "-"
          )
          : "-",
      },
    },
  };
};

function obterAssinantesD4sign(contract) {
  const signers = [];

  if (contract?.formaAssinatura === 1 && contract?.assEmailPessoal)
    signers.push({
      email: contract?.assEmailPessoal,
      act: "1",
      foreign: "0",
      certificadoicpbr: "0",
      assinatura_presencial: "0",
      docauth: "0",
      docauthandselfie: "0",
      embed_methodauth: "email",
      embed_smsnumber: "",
      upload_allow: "0",
      upload_obs: "Contrato Coobrastur",
    });

  if (
    contract?.terceiro &&
    contract?.formaAssinaturaTerceiro === 1 &&
    contract?.emailTerceiro
  )
    signers.push({
      email: contract?.emailTerceiro,
      act: "1",
      foreign: "0",
      certificadoicpbr: "0",
      assinatura_presencial: "0",
      docauth: "0",
      docauthandselfie: "0",
      embed_methodauth: "email",
      embed_smsnumber: "",
      upload_allow: "0",
      upload_obs: "Contrato Coobrastur",
    });

  if (
    contract?.fiador &&
    contract?.formaAssinaturaFiador === 1 &&
    contract?.emailFiador
  )
    signers.push({
      email: contract?.fiaEmailPessoal,
      act: "1",
      foreign: "0",
      certificadoicpbr: "0",
      assinatura_presencial: "0",
      docauth: "0",
      docauthandselfie: "0",
      embed_methodauth: "email",
      embed_smsnumber: "",
      upload_allow: "0",
      upload_obs: "Contrato Coobrastur",
    });

  return signers;
}

function obterTemplateD4sign(contract) {
  if (!contract.fiador && !contract.aditamento && !contract.terceiro)
    return 'MTkzMzM';

  if (contract.fiador && !contract.aditamento && !contract.terceiro)
    return 'MTE4MzM';

  if (!contract.fiador && !contract.aditamento && contract.terceiro)
    return 'MTk0Mzg';

  if (
    !contract.fiador &&
    !contract.terceiro &&
    contract.aditamento &&
    contract.formaAditamento == 1
  )
    return 'MTQzNDM';
  if (
    !contract.fiador &&
    !contract.terceiro &&
    contract.aditamento &&
    contract.formaAditamento == 2
  )
    return 'MTQzNDQ';
};

module.exports = {
  setD4SignStatus,
  verificarVendasNaoEnviadas,
  enviarVenda
}