import {
  restricaoArray,
  booleanArray,
  ufArray,
  tipoVendaArray,
  planoArray,
  planoDescricaoArray,
  diariasArray,
  diariasGoArray,
  bandeiraCartaoArray,
  formaPagamentoMensalidadeArray,
  diaMensalidadeBancoArray,
  diaMensalidadeCartaoArray,
  bancoContaArray,
  bancoCaixaTipoArray,
  estadoCivilArray,
  formaAssinaturaArray,
  adesaoMeioArray,
  formaPagamentoAdesaoVendedorArray,
  formaPagamentoAdesaoCoobrasturArray,
  parcelasPagamentoAdesaoArray,
  formaAditamentoArray,
  valorMensalidadePlanoFamiliaArray,
  valorMensalidadePlanoArray,

} from "./datas";

const {
  lastDayOfMonth,
  differenceInYears,
  isValid,
  parse,
  isAfter,
  isBefore,
  setDate,
  differenceInDays,
  addMonths,
  subMonths,
  addYears,
  subYears,
  format,
  getYear,
} = require('date-fns');

const ptBr = require('date-fns/locale/pt-BR');

const converterNumeroCaixa = numero => {
  if (!numero) return '';

  // se o numero for 1234
  // entao essa funcao ira preencher com zeros a esquerda
  // retornando entao 00001234
  // porem essa funcionalidade foi inativa
  // entao só retorno o numero
  // caso essa regra for inativada, basta adicionar "return numero" abaixo.
  // return numero;

  const completarDigitos = 8;

  if (String(numero).length >= completarDigitos) return String(numero);

  const digitosMaximo = completarDigitos;
  const digitosNumero = String(numero).length;
  const digitosTotais = '000000000';

  const formulaSlice = digitosMaximo - digitosNumero;
  const digitosZeroEsquerda = digitosTotais.slice(0, formulaSlice);

  const digitosFormatados = String(digitosZeroEsquerda + numero);
  return digitosFormatados;
};

const getLimiteDigitosConta = ({ banco }) => {
  switch (Number(banco)) {
    case 1:
      // label: "001-B. BRASIL" },
      return 7;

    case 33:
      // label: "033-SANTANDER" },
      return 9;

    case 41:
      // label: "041-BANRISUL" },
      return 9;

    case 104:
      // label: "104-CAIXA" },
      // 2 tipo + 8 digitos conta + 1 dv
      return 8;

    case 237:
      // label: "237-BRADESCO" },
      return 7;

    case 341:
      // label: "341-ITAÚ" },
      return 5;

    case 748:
      // label: "748-SICREDI" },
      return 5;

    default:
      return;
  }
};

const getLimiteDigitosAgencia = ({ banco }) => {
  switch (Number(banco)) {
    // label: "001-B. BRASIL" },
    case 1:
      return 4;

    // label: "033-SANTANDER" },
    case 33:
      return 4;

    // label: "041-BANRISUL" },
    case 41:
      return 4;

    // label: "104-CAIXA" },
    case 104:
      return 4;

    // label: "237-BRADESCO" },
    case 237:
      return 4;

    // label: "341-ITAÚ" },
    case 341:
      return 4;

    // label: "748-SICREDI" },
    case 748:
      return 4;

    default:
      return;
  }
};

const getMensalidade = ({ plano, diarias, familia }) => {
  return new Promise((resolve, reject) => {
    global.sql.request()
      .input('in_tpplcodigo', plano)
      .input('in_plFamilia', familia)
      .input('in_QtdeDiarias', diarias)
      .input('in_mentpCodigo', 2) //adesao = 1, mensalidade = 2
      .execute('siscoob.dbo.sp_WSROL_Compra_Online_Associados_Valor_Pgto_C2_v3')
      .then((result) => handleResult(result))
      .catch(err => { res.status(403).json(sqlServerUtils.errorExtractor(err)) });

    const handleResult = (result) => {
      if (result?.recordset[0]?.ValorPgto) {
        resolve(result.recordset[0].ValorPgto);
      } else {
        reject('Valor de mensalidade não encontrado.');
      }
      
    }

  });


};

const getDayFromString = dateStr => {
  return String(dateStr.slice(0, 2));
};

const getMonthFromString = dateStr => {
  return String(dateStr.slice(2, 4));
};

const getYearFromString = dateStr => {
  return String(dateStr.slice(4, 8));
};

const getDataPrimeiraMensalidade = ({
  adesao,
  meioAdesao,
  parcelasAdesaoCoobrastur,
  parcelasAdesaoVendedor,
  dataCoobrastur,
  dataVendedor,
  formaPagamento,
  dataDebitoConta,
  dataFaturaCartao,
}) => {
  if (!adesao) {
    const dataStr = formaPagamento == 1 ? dataDebitoConta : dataFaturaCartao;

    if (!dataStr) return false;
    if (dataStr.length !== 8) return false;

    const dia = dataStr.slice(0, 2);
    const mes = dataStr.slice(2, 4);
    const ano = dataStr.slice(4, 8);

    const dataCobranca = new Date(ano, mes - 1, dia);

    const month = format(dataCobranca, 'MMM', { locale: ptBr });
    const year = getYear(dataCobranca);

    return `${month}/${year}`;
  }

  const parcelas =
    meioAdesao === 1 ? parcelasAdesaoVendedor : parcelasAdesaoCoobrastur;
  const dataStr = meioAdesao === 1 ? dataVendedor : dataCoobrastur;

  if (!dataStr) return false;
  if (dataStr.length !== 8) return false;

  const dia = dataStr.slice(0, 2);
  const mes = dataStr.slice(2, 4);
  const ano = dataStr.slice(4, 8);

  const dataCobranca = new Date(ano, mes - 1, dia);
  const dataPrimeiraMensalidade = addMonths(new Date(dataCobranca), parcelas);

  const month = format(new Date(dataPrimeiraMensalidade), 'MMM', {
    locale: ptBr,
  });
  const year = getYear(new Date(dataPrimeiraMensalidade));

  return `${month}/${year}`;
};

const getDataLiberacaoAditamento = ({
  adesao,
  meioAdesao,
  parcelasAdesaoCoobrastur,
  parcelasAdesaoVendedor,
  dataCoobrastur,
  dataVendedor,
}) => {
  if (!adesao) {
    const currentDate = new Date();

    const dataLiberacao = addYears(new Date(currentDate), 1);

    const month = format(dataLiberacao, 'MMM', { locale: ptBr });
    const year = getYear(dataLiberacao);

    return `${month}/${year}`;
  }

  const parcelas =
    meioAdesao === 1 ? parcelasAdesaoVendedor : parcelasAdesaoCoobrastur;
  const dataStr = meioAdesao === 1 ? dataVendedor : dataCoobrastur;

  if (!dataStr) return false;

  if (dataStr.length !== 8) return false;

  const dia = dataStr.slice(0, 2);
  const mes = dataStr.slice(2, 4);
  const ano = dataStr.slice(4, 8);

  const data = new Date(ano, mes, dia);
  const dataPrimeiraMensalidade = addMonths(new Date(data), parcelas - 1);

  const dataLiberacao = addYears(new Date(dataPrimeiraMensalidade), 1);

  const month = format(new Date(dataLiberacao), 'MMM', {
    locale: ptBr,
  });
  const year = getYear(new Date(dataLiberacao));

  return `${month}/${year}`;
};

const getParcelasAdesaoBoletoCoobrasturArray = valor => {
  const parcelas = [
    { value: 1, label: 'À vista' },
    { value: 2, label: '2x' },
    { value: 3, label: '3x' },
  ];

  if (valor / 3 >= 100) return parcelas;
  if (valor / 2 >= 100) return [parcelas[0], parcelas[1]];

  return [parcelas[0]];
};

const labelByValue = (value, array) => {
  const filtered = array.filter(item => item.value == value && item);
  return filtered[0]?.label || '-';
};

const datebyString = string => {
  if (string.length !== 8) return false;

  const dataStr = string;

  const dia = dataStr.slice(0, 2);
  const mes = dataStr.slice(2, 4);
  const ano = dataStr.slice(4, 8);

  return new Date(ano, mes, dia);
};

const formatReal = valor => {
  const number = Number(valor);
  const formatedNumber = number
    .toFixed(2)
    .replace('.', ',')
    .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');

  return 'R$ ' + formatedNumber;
};

const checkCpf = cpf => {
  const strCPF = cpf.replace(/[^\d]+/g, '');
  var Soma;
  var Resto;

  Soma = 0;
  if (strCPF == '00000000000') return false;

  for (let n = 1; n <= 9; n++)
    Soma = Soma + parseInt(strCPF.substring(n - 1, n)) * (11 - n);
  Resto = (Soma * 10) % 11;

  if (Resto == 10 || Resto == 11) Resto = 0;
  if (Resto != parseInt(strCPF.substring(9, 10))) return false;

  Soma = 0;
  for (let s = 1; s <= 10; s++)
    Soma = Soma + parseInt(strCPF.substring(s - 1, s)) * (12 - s);
  Resto = (Soma * 10) % 11;

  if (Resto == 10 || Resto == 11) Resto = 0;
  if (Resto != parseInt(strCPF.substring(10, 11))) return false;
  return true;
};

const checkCpfPr = cpf => {
  return new Promise((resolve, reject) => {

    const strCPF = cpf.replace(/[^\d]+/g, '');
    var Soma;
    var Resto;

    Soma = 0;
    if (strCPF == '00000000000') reject(false);

    for (let n = 1; n <= 9; n++)
      Soma = Soma + parseInt(strCPF.substring(n - 1, n)) * (11 - n);
    Resto = (Soma * 10) % 11;

    if (Resto == 10 || Resto == 11) Resto = 0;
    if (Resto != parseInt(strCPF.substring(9, 10))) reject(false);

    Soma = 0;
    for (let s = 1; s <= 10; s++)
      Soma = Soma + parseInt(strCPF.substring(s - 1, s)) * (12 - s);
    Resto = (Soma * 10) % 11;

    if (Resto == 10 || Resto == 11) Resto = 0;
    if (Resto != parseInt(strCPF.substring(10, 11))) reject(false);
    resolve(true);
  })
};

const checkStringDate = date => {
  if (date === undefined/*  || date.length !== 8 */) return (false);

  date = parse(date, 'dd/MM/yyyy', new Date());
  return isValid(date);
};

const checkStringDatePr = date => {
  return new Promise((resolve, reject) => {
    if (date === undefined/*  || date.length !== 8 */) resolve(false);

    date = parse(date, 'dd/MM/yyyy', new Date());
    resolve(isValid(date));
  })
};

const checkDataCobrancaPr = (date, msg) => {

  return new Promise((resolve, reject) => {

    if (!date) reject(msg + ' precisa ter uma data, por favor preencher.');

    if (!checkStringDate(date)) {
      reject(msg + ' precisa ter uma data de cobrança válida, por favor corrigir.');
    }


    date = date.replace(/\//g, '');
    const dia = date.slice(0, 2);
    const mes = date.slice(2, 4);
    const ano = date.slice(4, 8);

    const anoMax = getYear(new Date()) + 2;

    if (dia < 1 || dia > 31) reject(`${msg} precisa ter dia de cobrança válido.`);
    if (mes < 1 || mes > 12) reject(`${msg} precisa ter mês de cobrança válido.`);
    if (ano > anoMax) reject(`${msg} precisa ter o ano menor que ${anoMax}.`);

    const currentDate = new Date();

    const currentYear = currentDate.getFullYear();
    const currentMonth = currentDate.getMonth();
    const currentDay = currentDate.getDate();

    const dataAtual = new Date(currentYear, currentMonth, currentDay, 0, 0, 0, 0);

    const dataAtualAcrescida = new Date(addMonths(dataAtual, 3));
    const dataLimite = new Date(lastDayOfMonth(dataAtualAcrescida));

    const dataCobranca = new Date(ano, mes - 1, dia, 0, 0, 0, 0);
    if (!isValid(dataCobranca)) {
      reject(`${msg} precisa ter uma data de cobrança válida.`);
    }


    const minimo = isBefore(dataCobranca, dataAtual);
    const maximo = isAfter(dataCobranca, dataLimite);

    if (minimo) { reject(`${msg} não pode ser anterior a data atual.`) };
    if (maximo) { reject(`${msg} não pode exceder 3 meses da data atual.`) };

    resolve();
  })
};



const checkMaiorIdade = (date, msg) => {
  if (date.length !== 8)
    throw `${msg} precisa ter uma data de nascimento válida.`;

  const dia = date.slice(0, 2);
  const mes = date.slice(2, 4);
  const ano = date.slice(4, 8);

  if (dia < 1 || dia > 31) throw `${msg} precisa ter dia de nascimento válido.`;
  if (mes < 1 || mes > 12) throw `${msg} precisa ter mês de nascimento válido.`;

  const dataAtual = new Date();
  const dataNascimento = new Date(ano, mes - 1, dia);

  if (!isValid(dataNascimento))
    throw `${msg} precisa ter uma dada de nascimento válida.`;

  const idade = differenceInYears(dataAtual, dataNascimento);

  if (idade < 18) throw `${msg} precisa ter mais de 18 anos.`;
  if (idade > 130) throw `${msg} precisa ter menos de 130 anos.`;
};


const checkMaiorIdadePr = (date, msg) => {
  return new Promise((resolve, reject) => {
    if (date.length !== 8 && date.length !== 10)
      reject(`${msg} precisa ter uma data de nascimento válida.`);

    const dataNascimento = parse(date, 'dd/MM/yyyy', new Date());
    const dataAtual = new Date();
    if (!isValid(dataNascimento)) {
      reject(`${msg} precisa ter uma data de nascimento válida.`);
    };

    const idade = differenceInYears(dataAtual, dataNascimento);

    if (idade < 18) reject(`${msg} precisa ter mais de 18 anos.`);
    if (idade > 130) reject(`${msg} precisa ter menos de 130 anos.`);

    resolve()

    /* 
        const dia = date.slice(0, 2);
        const mes = date.slice(2, 4);
        const ano = date.slice(4, 8);
    
        if (dia < 1 || dia > 31) reject(`${msg} precisa ter dia de nascimento válido.`)
        if (mes < 1 || mes > 12) reject(`${msg} precisa ter mês de nascimento válido.`);
    
        const dataAtual = new Date();
        const dataNascimento = new Date(ano, mes - 1, dia);
        if (!isValid(dataNascimento))
          throw `${msg} precisa ter uma data de nascimento válida.`; 
    
        const idade = differenceInYears(dataAtual, dataNascimento);
    
        if (idade < 18) reject(`${msg} precisa ter mais de 18 anos.`);
        if (idade > 130) reject(`${msg} precisa ter menos de 130 anos.`);*/
  })
};

export const validateValues = async (contract) => {
  return new Promise(async (resolve, reject) => {
    // dados pessoais
    if (!contract.assCPF_CNPJ) {
      reject("Preencha o CPF do associado")
    }

    try {
      await checkCpfPr(contract.assCPF_CNPJ)
    } catch (err) {
      reject("Por favor, preencha um CPF válido para o novo associado.");
    }

    if (!contract.assNome_RazaoSocial) {
      reject("Preencha o nome do associado");
    }

    // contato
    if (!contract.assEmailPessoal) { reject("Preencha o email do associado"); }
    if (!contract.assNumCelularDDD) { reject("Preencha o DDD do celular do associado"); }
    if (!contract.assNumCelular) { reject("Preencha o celular do associado"); }


    // indicação dados pessoais
    if (contract.indicacao === undefined) { reject("Selecione uma opção para indicador"); }

    if (contract.indicacao) {
      if (!contract.cpfIndicador) { reject(`Preencha o CPF do indicador`); }

      if (contract?.cpf === contract?.cpfIndicador) { reject("O CPF do indicador não pode ser igual ao CPF do novo associado."); }

      if (contract.cpfIndicador) {

        try {
          await checkCpfPr(contract.cpf_Indicador)
        } catch (err) {
          reject("Por favor, preencha um CPF válido para o indicador.");
        }

        if (contract?.nome === contract?.nomeIndicador) { reject("O nome do indicador não pode ser igual ao nome do novo associado."); }

        if (contract?.email === contract?.emailIndicador) { reject("O e-mail do indicador não pode ser igual ao e-mail do novo associado."); }

        if (contract?.assNumCelular === contract?.celularIndicador) { reject("O celular do indicador não pode ser igual ao celular do novo associado."); }

      }
    }





    // restricao
    if (contract.assRestricao === undefined) { reject("Selecione uma opção para restrição do associado"); }

    if (contract.assRestricao) {

      //TODO: Saber se esse campo existe no front
      /*  if (!contract.alternativaRestricao)
         throw "Selecione uma alternativa para restricao do associado"; */

      // aditamento
      if (contract.aditamento) {
        if (!contract.formaAditamento) { reject("Selecione uma forma de aditamento"); }
      }

      // fiador
      if (contract.fiador) {
        if (!contract.fiaCpf) { reject("Preencha o CPF do fiador"); }

        if (contract?.assCPF_CNPJ === contract?.fiaCpf) { reject("O CPF do fiador não pode ser igual ao CPF do novo associado."); }

        try {
          await checkCpfPr(contract.fiaCpf)
        } catch (err) {
          reject("Por favor, preencha um CPF válido para o fiador.");
        }

        if (!contract.fianome) { reject("Preencha o nome do fiador"); }
        if (!contract.fiaDtNascimento) { reject("Preencha o nascimento do fiador"); }

        try {
          await checkStringDatePr(contract.fiaDtNascimento)
        } catch (err) {
          reject("A data de nascimento do fiador é inválida, por favor corrigir.");
        }


        try {
          await checkMaiorIdadePr(contract.fiaDtNascimento.replace(/\\/g, ''), "O fiador");
        } catch (err) {
          reject(err);
        }

        if (!contract.fianacionalidade) { reject("Preencha a nacionalidade do fiador"); }
        if (!contract.civCodigo) { reject("Selecione o estado civil do fiador"); }

        if (contract.civCodigo === 2) {
          if (!contract.fiaConjuge) { reject("Preencha o nome do conjuge do fiador"); }
          if (!contract.fiaConjugeFiliacao) { reject("Preencha o nome da mãe do fiador"); }

        }

        if (!contract.fiaEmailPessoal) { reject("Preencha o email do fiador"); }

        if (contract?.assEmailPessoal === contract?.fiaEmailPessoal) { reject("O e-mail do fiador não pode ser igual ao e-mail do novo associado."); }

        if (!contract.fiaNumeroCelularDDD) { reject("Preencha o celular do fiador"); }
        if (!contract.fiaNumeroCelular) { reject("Preencha o celular do fiador"); }

        if (contract?.assNumCelular === contract?.celularFiador) { reject("O celular do fiador não pode ser igual ao celular do novo associado."); }

        if (!contract.formaAssinaturaFiador) { reject("Selecione uma forma de assinatura para o fiador"); }

        if (contract.formaAssinaturaFiador === 2) {
          if (!contract.assinaturaFiador) {
            reject("Na opção de assinatura offline(dedo), o fiador precisa assinar");
          }
        }
      }
    }

    // associação / dados venda
    if (!contract.tipoVenda) { reject("Selecione o tipo de contrato do associado"); }

    if (contract.tipoVenda == 2) {
      if (contract.adesao) { reject(`O tipo de venda "reativação" não permite cobrança de adesão.`); }
    }

    if (!contract.tpplcodigo) { reject("Selecione o plano do associado"); }
    if (!contract.plQtdeDiarias) { reject("Selecione as diárias do plano"); }
    if (contract.plfamilia === undefined) { reject("Selecione uma opção para plano familia"); }

    // adesão
    if (contract.adesao === undefined) { reject("Selecione uma opção para adesão"); }

    if (contract.adesao) {
      if (!contract.valorAdesao || contract.valorAdesao < 1) { reject("Preencha o valor da adesão"); }

      if (!contract.meioAdesao) { reject("Selecione o meio de pagamento da adesão"); }

      if (contract.meioAdesao == 1) {
        if (!contract.formaAdesaoVendedor) { reject("Selecione o forma de pagamento da adesão ao vendedor"); }
        if (!contract.formaAdesaoVendedor) //parcelasAdesaoVendedor
        { reject("Selecione a forma de parcelamento do pagamento da adesão ao vendedor"); }
        if (!contract.dataVendedor) { reject("Preencha o data de cobrança do pagamento da adesão ao vendedor"); }

        try {
          await checkDataCobrancaPr(
            contract.dataVendedor,
            "Pagamento da adesão ao vendedor"
          );
        } catch (error) {
          reject(error);
        }
      }

      if (contract.meioAdesao == 2) {
        if (!contract.formaAdesaoCoobrastur) { reject("Selecione o forma de pagamento da adesão a Coobrastur"); }

        if (!contract.parcelasAdesaoCoobrastur) { reject("Selecione a forma de parcelamento do pagamento da adesão a Coobrastur"); }

        if (!contract.dataCoobrastur) { reject("Preencha o data de cobrança do pagamento da adesão a Coobrastur"); }

        try {
          await checkDataCobranca(
            contract.dataCoobrastur,
            "Pagamento da adesão a Coobrastur"
          );
        } catch (error) {
          reject(error);
        }
      }
    }

    // endereço
    if (!contract.endUF) { reject("Selecione o uf do associado"); }

    if (contract.adesao && contract.meioAdesao == 2) {
      if (!contract.endCepAssociado) { reject("Preencha o CEP do associado"); }
      if (!contract.endCidadeAssociado) { reject("Preencha a cidade do endereço do associado"); }
      if (!contract.endBairroAssociado) { reject("Preencha o bairro do endereço do associado"); }
      if (!contract.endLogradouroAssociado) { reject("Preencha a rua do endereço do associado"); }
      if (!contract.endNumLogradouroAssociado) { reject("Preencha o número do endereço do associado"); }
    }

    // pagamento
    //MOVIDO PARA checkPagamento(), já que agora os dados de pagamento vão ser inseridos pelo cliente

    // Terceiro
    if (contract.terceiro) {
      if (!contract.cpfTerceiro) { reject("Preencha o CPF do terceiro"); }

      if (!checkCpf(contract.cpfTerceiro)) { reject("Por favor, preencha um CPF válido para o terceiro."); }

      if (!contract.nomeTerceiro) { reject("Preencha o nome completo do terceiro"); }
      if (!contract.nascimentoTerceiro) { reject("Preencha o nascimento do terceiro"); }

      if (!checkStringDate(contract?.nascimentoTerceiro)) { reject("A data de nascimento do terceiro é inválida, por favor corrigir."); }

      try {
        await checkMaiorIdadePr(contract.nascimentoTerceiro, "O terceiro");
      } catch (err) {
        reject(err);
      }

      if (!contract.nacionalidadeTerceiro) { reject("Preencha o nacionalidade do terceiro"); }
      if (!contract.estadoCivilTerceiro) { reject("Selecione o estado civil do terceiro"); }

      if (contract.estadoCivilTerceiro == 2) {
        if (!contract.nomeConjugeTerceiro) { reject("Preencha o nome do cônjuge do terceiro"); }
      }

      if (!contract.nomeMaeTerceiro) { reject("Preencha o nome da mãe do terceiro"); }

      if (!contract.emailTerceiro) { reject("Preencha o e-mail do terceiro"); }

      if (contract?.email === contract?.emailTerceiro) { reject("O e-mail do terceiro não pode ser igual ao e-mail do novo associado."); }

      if (!contract.dddCelularTerceiro) { reject("Preencha o DDD do celular do terceiro"); }
      if (!contract.numCelularTerceiro) { reject("Preencha o celular do terceiro"); }

      if (contract?.assNumCelular === contract?.numCelularTerceiro) { reject("O celular do terceiro não pode ser igual ao celular do novo associado."); }

      if (!contract.formaAssinaturaTerceiro) { reject("Selecione uma opção para a forma de assinatura do terceiro"); }

      if (contract.formaAssinaturaTerceiro === 2) {
        if (!contract.assinaturaTerceiro) { reject("Na opção de assinatura offline(dedo), o terceiro precisa assinar"); }
      }
    }

    // adesao via cartao e pagamento mensalidade via conta
    //TODO: Implementar nova função de pagamento inserido por cliente
    /* if (contract.formaAdesaoCoobrastur == 2 && contract.formaPagamento == 1) {
      if (!contract.cpfTitularCartao)
        throw "Preencha o CPF do titular do cartão para pagamento de mensalidades";
  
      if (!checkCpf(contract.cpfTitularCartao))
        throw "Por favor, preencha um CPF válido para o titular do cartão para pagamento de mensalidades.";
  
      if (!contract.nomeTitularCartao)
        throw "Preencha o nome titular do cartão para pagamento de mensalidades";
      if (!contract.numeroCartao)
        throw "Preencha o número do cartão  para pagamento de mensalidades";
      if (!contract.mesValidadeCartao)
        throw "Preencha o mês de validade do cartão para pagamento de mensalidades";
  
      if (String(contract.mesValidadeCartao).length < 2)
        throw "Preencha o mês de validade do cartão para pagamento de mensalidades com dois números";
  
      if (
        Number(contract.mesValidadeCartao) < 1 ||
        Number(contract.mesValidadeCartao) > 31
      )
        throw "Preencha um mês de validade do cartão válido";
  
      if (!contract.anoValidadeCartao)
        throw "Preencha o ano de validade do cartão  para pagamento de mensalidades";
  
      if (contract.anoValidadeCartao.length !== 4)
        throw "Preencha um ano de validade com 4 dígitos.";
  
      if (Number(contract.anoValidadeCartao) < new Date().getFullYear())
        throw "Preencha um ano de validade do cartão válido";
  
      if (
        isBefore(
          new Date(
            Number(contract.anoValidadeCartao),
            Number(contract.mesValidadeCartao) - 1,
            1
          ),
          setDate(new Date(), 1)
        )
      )
        throw "O cartão de crédito não pode estar vencido";
  
      if (!contract.cvvCartao)
        throw "Preencha o CVV do cartão para pagamento de mensalidades";
    } */

    // ASSINATURA
    if (!contract.formaAssinatura) {
      reject("Selecione uma forma de assinatura para o associado");
    }

    if (contract.formaAssinatura === 2) {
      if (!contract.assinatura) {
        reject("Na opção de assinatura offline(dedo), o associado precisa assinar");
      }
    }

    resolve();

  })
};

const checkPagamento = contract => {
  if (!contract.formaPagamento)
    throw "Selecione a forma de pagamento da mensalidade do plano";

  if (contract.formaPagamento == 1) {
    if (!contract.cpfTitularContaCorrente)
      throw "Preencha o CPF do titular da conta corrente para pagamento de mensalidades";

    if (!checkCpf(contract.cpfTitularContaCorrente))
      throw "Por favor, preencha um CPF válido para o titular da conta corrente para pagamento de mensalidades.";

    if (!contract.nomeTitularContaCorrente)
      throw "Preencha o nome titular da conta corrente para pagamento de mensalidades";
    if (!contract.bancoContaCorrente)
      throw "Selecione o banco da conta corrente para pagamento de mensalidades";

    if (!contract.numeroContaCorrente)
      throw "Preencha o número da conta corrente para pagamento de mensalidades";

    // banco 1 e 104 não exigem número específico de dígitos
    if (
      Number(contract.bancoContaCorrente) !== 1 &&
      Number(contract.bancoContaCorrente) !== 104
    ) {
      const digitos = String(contract.numeroContaCorrente).length;
      const digitosNecessarios = getLimiteDigitosConta({
        banco: Number(contract.bancoContaCorrente),
      });

      if (digitos !== digitosNecessarios)
        throw `O número da conta corrente tem ${digitos} dígitos, mas deve conter ${digitosNecessarios} dígitos. Por favor, corrigir.`;
    }

    // se banco caixa 104, numero não pode exceder a 8
    if (Number(contract.bancoContaCorrente) === 104) {
      const digitosCaixa = String(contract.numeroContaCorrente).length;
      const digitosLimiteCaixa = getLimiteDigitosConta({
        banco: Number(contract.bancoContaCorrente),
      });

      if (digitosCaixa > digitosLimiteCaixa)
        throw `O número da conta corrente tem ${digitosCaixa} dígitos, mas deve conter até ${digitosLimiteCaixa} dígitos. Por favor, corrigir.`;
    }

    if (!contract.dvContaCorrente)
      throw "Preencha o dígito da conta corrente para pagamento de mensalidades";

    if (!contract.agenciaContaCorrente)
      throw "Preencha a agência da conta corrente para pagamento de mensalidades";

    const digitosAgencia = String(contract.agenciaContaCorrente).length;
    const digitosAgenciaNecessarios = getLimiteDigitosAgencia({
      banco: Number(contract.bancoContaCorrente),
    });

    if (digitosAgencia !== digitosAgenciaNecessarios)
      throw `O número da agência tem ${digitosAgencia} dígitos, mas deve conter ${digitosAgenciaNecessarios} dígitos. Por favor, corrigir.`;

    if (contract.bancoContaCorrente == 41) {
      if (!contract.dvAgenciaContaCorrente)
        throw "Preencha o dígito agência da conta corrente para pagamento de mensalidades";

      const digitosDvAgencia = String(contract.dvAgenciaContaCorrente).length;
      const digitosDvAgenciaNecessarios = 2;

      if (digitosDvAgencia !== digitosDvAgenciaNecessarios)
        throw `O DV da agência tem ${digitosDvAgencia} dígitos, mas deve conter ${digitosDvAgenciaNecessarios} dígitos. Por favor, corrigir.`;
    }

    if (contract.adesao) {
      if (!contract.diaDebitoConta)
        throw "Selecione o dia para débito em conta corrente para pagamento de mensalidades";
    } else {
      if (!contract.dataDebitoConta)
        throw "Preencha a data para débito em conta corrente para pagamento de mensalidades";

      checkDataCobranca(
        contract.dataDebitoConta,
        "Pagamento da mensalidade via débito em conta"
      );

      const diaDebitoConta = getDayFromString(contract.dataDebitoConta);

      if (diaDebitoConta % 5 > 0) {
        throw "Preencha a data para débito com dia 05, 10, 15, 20, 25 ou 30 em conta corrente para pagamento de mensalidades";
      }
    }
  }

  if (contract.formaPagamento == 2 || contract.formaPagamento == 3) {
    if (!contract.cpfTitularCartao)
      throw "Preencha o CPF do titular do cartão para pagamento de mensalidades";

    if (!checkCpf(contract.cpfTitularCartao))
      throw "Por favor, preencha um CPF válido para o titular do cartão para pagamento de mensalidades.";

    if (!contract.nomeTitularCartao)
      throw "Preencha o nome titular do cartão para pagamento de mensalidades";
    if (!contract.numeroCartao)
      throw "Preencha o número do cartão  para pagamento de mensalidades";
    if (!contract.mesValidadeCartao)
      throw "Preencha o mês de validade do cartão para pagamento de mensalidades";

    if (String(contract.mesValidadeCartao).length < 2)
      throw "Preencha o mês de validade do cartão para pagamento de mensalidades com dois números";

    if (
      Number(contract.mesValidadeCartao) < 1 ||
      Number(contract.mesValidadeCartao) > 31
    )
      throw "Preencha um mês de validade do cartão válido";

    if (!contract.anoValidadeCartao)
      throw "Preencha o ano de validade do cartão  para pagamento de mensalidades";

    if (contract.anoValidadeCartao.length !== 4)
      throw "Preencha um ano de validade com 4 dígitos.";

    if (Number(contract.anoValidadeCartao) < new Date().getFullYear())
      throw "Preencha um ano de validade do cartão válido";

    if (
      isBefore(
        new Date(
          Number(contract.anoValidadeCartao),
          Number(contract.mesValidadeCartao) - 1,
          1
        ),
        setDate(new Date(), 1)
      )
    )
      throw "O cartão de crédito não pode estar vencido";

    if (!contract.cvvCartao)
      throw "Preencha o CVV do cartão para pagamento de mensalidades";

    if (contract.adesao) {
      if (!contract.diaFaturaCartao)
        throw "Selecione o dia da fatura do cartão para pagamento de mensalidades";
    } else {
      if (!contract.dataFaturaCartao)
        throw "Preencha a data de cobrança das mensalidades via cartão de crédito";

      checkDataCobranca(
        contract.dataFaturaCartao,
        "Pagamento da mensalidade via cartão"
      );
    }
  }
}

const validateValuesEn = contract => {
  // dados pessoais
  if (!contract.assCPF_CNPJ) throw 'Preencha o CPF do associado';

  if (!checkCpf(contract.assCPF_CNPJ))
    throw 'Por favor, preencha um CPF válido para o novo associado.';

  if (!contract.assNome_RazaoSocial) throw 'Preencha o nome do associado';

  // contato
  if (!contract.assEmailPessoal) throw 'Preencha o email do associado';

  if (!contract.numCelularDDD) throw 'Preencha o DDD do celular do associado';
  if (!contract.assNumCelular) throw 'Preencha o celular do associado';


  // indicação dados pessoais
  if (contract.indicator === undefined)
    throw 'Selecione uma opção para indicador';

  if (contract.indicator) {
    if (!contract.cpfIndicator) throw `Preencha o CPF do indicador`;

    if (contract?.cpf === contract?.cpfIndicator)
      throw 'O CPF do indicador não pode ser igual ao CPF do novo associado.';

    if (contract.cpfIndicator)
      if (!checkCpf(contract.cpfIndicator))
        throw 'Por favor, preencha um CPF válido para o indicador.';

    if (contract?.name === contract?.nomeIndicator)
      throw 'O nome do indicador não pode ser igual ao nome do novo associado.';

    if (contract?.email === contract?.emailIndicator)
      throw 'O e-mail do indicador não pode ser igual ao e-mail do novo associado.';

    if (contract?.phoneNumber === contract?.phoneNumberIndicator)
      throw 'O celular do indicador não pode ser igual ao celular do novo associado.';
  }

  // restricao
  if (contract.restriction === undefined)
    throw 'Selecione uma opção para restrição do associado';

  if (contract.restriction) {
    if (!contract.alternativeRestriction)
      throw 'Selecione uma alternativa para restricao do associado';

    // aditamento
    if (contract.addition)
      if (!contract.typeAddition) throw 'Selecione uma forma de aditamento';

    // fiador
    if (contract.guarantor) {
      if (!contract.cpfGuarantor) throw 'Preencha o CPF do fiador';

      if (contract?.cpf === contract?.cpfGuarantor)
        throw 'O CPF do fiador não pode ser igual ao CPF do novo associado.';

      if (!checkCpf(contract.cpfGuarantor))
        throw 'Por favor, preencha um CPF válido para o fiador.';

      if (!contract.nameGuarantor) throw 'Preencha o nome do fiador';
      if (!contract.birthDateGuarantor) throw 'Preencha o nascimento do fiador';

      if (!checkStringDate(contract.birthDateGuarantor))
        throw 'A data de nascimento do fiador é inválida, por favor corrigir.';

      checkMaiorIdade(contract.birthDateGuarantor, 'O fiador');

      if (!contract.nationalityGuarantor)
        throw 'Preencha a nacionalidade do fiador';
      if (!contract.estadoCivilGuarantor)
        throw 'Selecione o estado civil do fiador';

      if (contract.maritalStatusGuarantor === 2)
        if (!contract.spouseGuarantor)
          throw 'Preencha o nome do conjuge do fiador';

      if (!contract.motherGuarantor) throw 'Preencha o nome da Mãe do fiador';

      if (!contract.fatherGuarantor) throw 'Preencha o nome do Pai do fiador';

      if (!contract.emailGuarantor) throw 'Preencha o email do fiador';

      if (contract?.email === contract?.emailGuarantor)
        throw 'O e-mail do fiador não pode ser igual ao e-mail do novo associado.';

      if (!contract.phoneNumberGuarantor) throw 'Preencha o celular do fiador';

      if (contract?.celular === contract?.phoneNumberGuarantor)
        throw 'O celular do fiador não pode ser igual ao celular do novo associado.';

      if (!contract.signatureFormGuarantor)
        throw 'Selecione uma forma de assinatura para o fiador';

      if (contract.signatureFormGuarantor === 2)
        if (!contract.signatureGuarantor)
          throw 'Na opção de assinatura offline(dedo), o fiador precisa assinar';
    }
  }

  // associação / dados venda
  if (!contract.typeSale) throw 'Selecione o tipo de contrato do associado';

  if (contract.typeSale == 2) {
    if (contract.adhesion)
      throw `O tipo de venda "reativação" não permite cobrança de adesão.`;
  }

  if (!contract.plan) throw 'Selecione o plano do associado';
  if (!contract.numberOfDaily) throw 'Selecione as diárias do plano';
  if (contract.family === undefined)
    throw 'Selecione uma opção para plano familia';

  // adesão
  if (contract.adhesion === undefined) throw 'Selecione uma opção para adesão';

  if (contract.adhesion) {
    if (!contract.valueAdhesion || contract.valueAdhesion < 1)
      throw 'Preencha o valor da adesão';

    if (!contract.halfAdhesion) throw 'Selecione o meio de pagamento da adesão';

    if (contract.halfAdhesion == 1) {
      if (!contract.sellerMembershipForm)
        throw 'Selecione o forma de pagamento da adesão ao vendedor';
      if (!contract.sellerAdhesionInstallments)
        throw 'Selecione a forma de parcelamento do pagamento da adesão ao vendedor';
      if (!contract.dateSeller)
        throw 'Preencha o data de cobrança do pagamento da adesão ao vendedor';

      checkDataCobranca(contract.dateSeller, 'Pagamento da adesão ao vendedor');
    }

    if (contract.halfAdhesion == 2) {
      if (!contract.coobrasturAdhesionForm)
        throw 'Selecione o forma de pagamento da adesão a Coobrastur';

      if (!contract.adhesionPlotsCoobrastur)
        throw 'Selecione a forma de parcelamento do pagamento da adesão a Coobrastur';

      if (!contract.dataCoobrastur)
        throw 'Preencha o data de cobrança do pagamento da adesão a Coobrastur';

      checkDataCobranca(
        contract.dataCoobrastur,
        'Pagamento da adesão a Coobrastur',
      );
    }
  }

  // endereço
  if (!contract.uf) throw 'Selecione o uf do associado';

  if (contract.adhesion && contract.halfAdhesion == 2) {
    if (!contract.zipCode) throw 'Preencha o CEP do associado';
    if (!contract.city) throw 'Preencha a cidade do endereço do associado';
    if (!contract.bairro) throw 'Preencha o bairro do endereço do associado';
    if (!contract.neighborhood) throw 'Preencha a rua do endereço do associado';
    if (!contract.houseNumber)
      throw 'Preencha o número do endereço do associado';
  }

  // pagamento
  if (!contract.typeSale)
    throw 'Selecione a forma de pagamento da mensalidade do plano';

  if (contract.typeSale == 1) {
    if (!contract.cpfCurrentAccountHolder)
      throw 'Preencha o CPF do titular da conta corrente para pagamento de mensalidades';

    if (!checkCpf(contract.cpfCurrentAccountHolder))
      throw 'Por favor, preencha um CPF válido para o titular da conta corrente para pagamento de mensalidades.';

    if (!contract.currentAccountHolderName)
      throw 'Preencha o nome titular da conta corrente para pagamento de mensalidades';
    if (!contract.bankCheckingAccount)
      throw 'Selecione o banco da conta corrente para pagamento de mensalidades';

    if (!contract.bankCheckingAccount)
      throw 'Preencha o número da conta corrente para pagamento de mensalidades';

    // banco 1 e 104 não exigem número específico de dígitos
    if (
      Number(contract.bankCheckingAccount) !== 1 &&
      Number(contract.bankCheckingAccount) !== 104
    ) {
      const digitos = String(contract.bankCheckingAccount).length;
      const digitosNecessarios = getLimiteDigitosConta({
        banco: Number(contract.bankCheckingAccount),
      });

      if (digitos !== digitosNecessarios)
        throw `O número da conta corrente tem ${digitos} dígitos, mas deve conter ${digitosNecessarios} dígitos. Por favor, corrigir.`;
    }

    // se banco caixa 104, numero não pode exceder a 8
    if (Number(contract.bankCheckingAccount) === 104) {
      const digitosCaixa = String(contract.bankCheckingAccount).length;
      const digitosLimiteCaixa = getLimiteDigitosConta({
        banco: Number(contract.bankCheckingAccount),
      });

      if (digitosCaixa > digitosLimiteCaixa)
        throw `O número da conta corrente tem ${digitosCaixa} dígitos, mas deve conter até ${digitosLimiteCaixa} dígitos. Por favor, corrigir.`;
    }

    if (!contract.currentAccountDigit)
      throw 'Preencha o dígito da conta corrente para pagamento de mensalidades';

    if (!contract.currentAccountDigit)
      throw 'Preencha a agência da conta corrente para pagamento de mensalidades';

    const digitosAgencia = String(contract.currentAccountDigit).length;
    const digitosAgenciaNecessarios = getLimiteDigitosAgencia({
      banco: Number(contract.bankCheckingAccount),
    });

    if (digitosAgencia !== digitosAgenciaNecessarios)
      throw `O número da agência tem ${digitosAgencia} dígitos, mas deve conter ${digitosAgenciaNecessarios} dígitos. Por favor, corrigir.`;

    if (contract.bankCheckingAccount == 41) {
      if (!contract.currentAccountDigit)
        throw 'Preencha o dígito agência da conta corrente para pagamento de mensalidades';

      const digitosDvAgencia = String(contract.currentAccountDigit).length;
      const digitosDvAgenciaNecessarios = 2;

      if (digitosDvAgencia !== digitosDvAgenciaNecessarios)
        throw `O DV da agência tem ${digitosDvAgencia} dígitos, mas deve conter ${digitosDvAgenciaNecessarios} dígitos. Por favor, corrigir.`;
    }

    if (contract.adhesion) {
      if (!contract.accountDebitDay)
        throw 'Selecione o dia para débito em conta corrente para pagamento de mensalidades';
    } else {
      if (!contract.accountDebitDate)
        throw 'Preencha a data para débito em conta corrente para pagamento de mensalidades';

      checkDataCobranca(
        contract.accountDebitDate,
        'Pagamento da mensalidade via débito em conta',
      );

      const diaDebitoConta = getDayFromString(contract.accountDebitDate);

      if (diaDebitoConta % 5 > 0) {
        throw 'Preencha a data para débito com dia 05, 10, 15, 20, 25 ou 30 em conta corrente para pagamento de mensalidades';
      }
    }
  }

  if (contract.typeSale == 2) {
    if (!contract.cpfCardHolder)
      throw 'Preencha o CPF do titular do cartão para pagamento de mensalidades';

    if (!checkCpf(contract.cpfCardHolder))
      throw 'Por favor, preencha um CPF válido para o titular do cartão para pagamento de mensalidades.';

    if (!contract.cardHolderName)
      throw 'Preencha o nome titular do cartão para pagamento de mensalidades';
    if (!contract.cardNumber)
      throw 'Preencha o número do cartão  para pagamento de mensalidades';
    if (!contract.cardExpiryMonth)
      throw 'Preencha o mês de validade do cartão para pagamento de mensalidades';

    if (String(contract.cardExpiryMonth).length < 2)
      throw 'Preencha o mês de validade do cartão para pagamento de mensalidades com dois números';

    if (
      Number(contract.cardExpiryMonth) < 1 ||
      Number(contract.cardExpiryMonth) > 31
    )
      throw 'Preencha um mês de validade do cartão válido';

    if (!contract.cardExpiryYear)
      throw 'Preencha o ano de validade do cartão  para pagamento de mensalidades';

    if (contract.cardExpiryYear.length !== 4)
      throw 'Preencha um ano de validade com 4 dígitos.';

    if (Number(contract.cardExpiryYear) < new Date().getFullYear())
      throw 'Preencha um ano de validade do cartão válido';

    if (
      isBefore(
        new Date(
          Number(contract.cardExpiryYear),
          Number(contract.cardExpiryMonth) - 1,
          1,
        ),
        setDate(new Date(), 1),
      )
    )
      throw 'O cartão de crédito não pode estar vencido';

    if (!contract.cvvCard)
      throw 'Preencha o CVV do cartão para pagamento de mensalidades';

    if (contract.adhesion) {
      if (!contract.cardInvoiceDay)
        throw 'Selecione o dia da fatura do cartão para pagamento de mensalidades';
    } else {
      if (!contract.cardInvoiceDate)
        throw 'Preencha a data de cobrança das mensalidades via cartão de crédito';

      checkDataCobranca(
        contract.cardInvoiceDate,
        'Pagamento da mensalidade via cartão',
      );
    }
  }

  // Terceiro
  if (contract.third) {
    if (!contract.cpfThird) throw 'Preencha o CPF do terceiro';

    if (!checkCpf(contract.cpfThird))
      throw 'Por favor, preencha um CPF válido para pagamento de mensalidades em conta corrente.';

    if (!contract.nameThird) throw 'Preencha o nome completo do terceiro';
    if (!contract.birthDateThird) throw 'Preencha o nascimento do terceiro';

    if (!checkStringDate(contract?.birthDateThird))
      throw 'A data de nascimento do terceiro é inválida, por favor corrigir.';

    checkMaiorIdade(contract.birthDateThird, 'O terceiro');

    if (!contract.nationalityThird)
      throw 'Preencha o nacionalidade do terceiro';
    if (!contract.maritalStatusThird)
      throw 'Selecione o estado civil do terceiro';

    if (contract.maritalStatusThird == 2)
      if (!contract.spouseThird) throw 'Preencha o nome do cônjuge do terceiro';

    if (!contract.motherThird) throw 'Preencha o nome da mãe do terceiro';

    if (!contract.emailThird) throw 'Preencha o e-mail do terceiro';

    if (contract?.email === contract?.emailThird)
      throw 'O e-mail do terceiro não pode ser igual ao e-mail do novo associado.';

    if (!contract.phoneNumberThird) throw 'Preencha o celular do terceiro';

    if (contract?.celular === contract?.phoneNumberThird)
      throw 'O celular do terceiro não pode ser igual ao celular do novo associado.';

    if (!contract.signatureFormThird)
      throw 'Selecione uma opção para a forma de assinatura do terceiro';

    if (contract.signatureFormThird === 2)
      if (!contract.signatureThird)
        throw 'Na opção de assinatura offline(dedo), o terceiro precisa assinar';
  }

  // adesao via cartao e pagamento mensalidade via conta
  if (contract.coobrasturAdhesionForm == 2 && contract.typeSale == 1) {
    if (!contract.cpfCardHolder)
      throw 'Preencha o CPF do titular do cartão para pagamento de mensalidades';

    if (!checkCpf(contract.cpfCardHolder))
      throw 'Por favor, preencha um CPF válido para o titular do cartão para pagamento de mensalidades.';

    if (!contract.cardHolderName)
      throw 'Preencha o nome titular do cartão para pagamento de mensalidades';
    if (!contract.cardNumber)
      throw 'Preencha o número do cartão  para pagamento de mensalidades';
    if (!contract.cardExpiryMonth)
      throw 'Preencha o mês de validade do cartão para pagamento de mensalidades';

    if (String(contract.cardExpiryMonth).length < 2)
      throw 'Preencha o mês de validade do cartão para pagamento de mensalidades com dois números';

    if (
      Number(contract.cardExpiryMonth) < 1 ||
      Number(contract.cardExpiryMonth) > 31
    )
      throw 'Preencha um mês de validade do cartão válido';

    if (!contract.cardExpiryYear)
      throw 'Preencha o ano de validade do cartão  para pagamento de mensalidades';

    if (contract.cardExpiryYear.length !== 4)
      throw 'Preencha um ano de validade com 4 dígitos.';

    if (Number(contract.cardExpiryYear) < new Date().getFullYear())
      throw 'Preencha um ano de validade do cartão válido';

    if (
      isBefore(
        new Date(
          Number(contract.cardExpiryYear),
          Number(contract.cardExpiryMonth) - 1,
          1,
        ),
        setDate(new Date(), 1),
      )
    )
      throw 'O cartão de crédito não pode estar vencido';

    if (!contract.cvvCard)
      throw 'Preencha o CVV do cartão para pagamento de mensalidades';
  }

  // ASSINATURA
  if (!contract.signatureForm)
    throw 'Selecione uma forma de assinatura para o associado';

  if (contract.signatureForm === 2)
    if (!contract.signature)
      throw 'Na opção de assinatura offline(dedo), o associado precisa assinar';

  return;
};

const genRef = () => {
  const dataAtual = new Date();
  const diaAtual = dataAtual.getUTCDate();

  const horaAtual = dataAtual.getUTCHours();
  const minAtual = dataAtual.getUTCMinutes();

  const letras = Math.random().toString(36).substr(2, 2);

  return `${letras}-${diaAtual}-${horaAtual}${minAtual}`;
};

const checkRef = (contract, contracts) => {
  const selected = contracts.filter(i => i.ref === contract.ref && i);
  if (!selected[0]) throw 'Referência de contrato não encontrada.';

  return;
};

const removeByRef = (contract, contracts) => {
  checkRef(contract, contracts);

  const filtered = contracts.filter(i => i.ref !== contract.ref && i);

  return filtered;
};

const masker = (value, mask) => {
  if (!value) return '';

  const exp = /\-|\.|\/|\(|\)|/g;
  const numbers = value.toString().replace(exp, '');

  if (numbers < 1) return;

  let index = 0;
  let maskedValue = '';
  let digits = numbers.length;

  for (let i = 0; i <= digits; i++) {
    const isMask = Boolean(
      mask.charAt(i) == '-' ||
      mask.charAt(i) == '.' ||
      mask.charAt(i) == '/' ||
      mask.charAt(i) == '(' ||
      mask.charAt(i) == ')' ||
      mask.charAt(i) == ' ',
    );

    maskedValue += isMask ? mask.charAt(i) : numbers.charAt(index);

    if (!isMask) index++;
    if (isMask) digits++;
  }

  return maskedValue;
};

function getSignersEnvioAssinaturas(contract) {
  const signers = [];

  if (contract?.formaAssinatura === 1 && contract?.assEmailPessoal)
    signers.push({
      email: contract?.assEmailPessoal,
      act: "1",
      foreign: "0",
      certificadoicpbr: "0",
      assinatura_presencial: "0",
      docauth: "0",
      docauthandselfie: "0",
      embed_methodauth: "email",
      embed_smsnumber: "",
      upload_allow: "0",
      upload_obs: "Contrato Coobrastur",
    });

  if (
    contract?.terceiro &&
    contract?.formaAssinaturaTerceiro === 1 &&
    contract?.emailTerceiro
  )
    signers.push({
      email: contract?.emailTerceiro,
      act: "1",
      foreign: "0",
      certificadoicpbr: "0",
      assinatura_presencial: "0",
      docauth: "0",
      docauthandselfie: "0",
      embed_methodauth: "email",
      embed_smsnumber: "",
      upload_allow: "0",
      upload_obs: "Contrato Coobrastur",
    });

  if (
    contract?.fiador &&
    contract?.formaAssinaturaFiador === 1 &&
    contract?.emailFiador
  )
    signers.push({
      email: contract?.fiaEmailPessoal,
      act: "1",
      foreign: "0",
      certificadoicpbr: "0",
      assinatura_presencial: "0",
      docauth: "0",
      docauthandselfie: "0",
      embed_methodauth: "email",
      embed_smsnumber: "",
      upload_allow: "0",
      upload_obs: "Contrato Coobrastur",
    });

  return signers;
}

const getD4signSigners = ({ contract }) => {
  const signers = [];

  if (contract?.formaAssinatura === 1 && contract?.assEmailPessoal)
    signers.push({
      email: contract?.email,
      act: '1',
      foreign: '0',
      certificadoicpbr: '0',
      assinatura_presencial: '0',
      docauth: '0',
      docauthandselfie: '0',
      embed_methodauth: 'email',
      embed_smsnumber: '',
      upload_allow: '0',
      upload_obs: 'Contrato Coobrastur',
    });

  if (
    contract?.terceiro &&
    contract?.formaAssinaturaTerceiro === 1 &&
    contract?.emailTerceiro
  )
    signers.push({
      email: contract?.emailTerceiro,
      act: '1',
      foreign: '0',
      certificadoicpbr: '0',
      assinatura_presencial: '0',
      docauth: '0',
      docauthandselfie: '0',
      embed_methodauth: 'email',
      embed_smsnumber: '',
      upload_allow: '0',
      upload_obs: 'Contrato Coobrastur',
    });

  if (
    contract?.fiador &&
    contract?.formaAssinaturaFiador === 1 &&
    contract?.fiaEmailPessoal
  )
    signers.push({
      email: contract?.fiaEmailPessoal,
      act: '1',
      foreign: '0',
      certificadoicpbr: '0',
      assinatura_presencial: '0',
      docauth: '0',
      docauthandselfie: '0',
      embed_methodauth: 'email',
      embed_smsnumber: '',
      upload_allow: '0',
      upload_obs: 'Contrato Coobrastur',
    });

  return signers;
};

module.exports = {
  checkCpf,
  checkStringDate,
  checkMaiorIdade,
  validateValues,
  getSignersEnvioAssinaturas,
  getD4signSigners,
  format,
  formatReal,
  labelByValue,
  getMensalidade
};
