FROM node:14

WORKDIR /app

COPY . .

ENV PORT=5005

RUN npm install

EXPOSE $PORT

ENTRYPOINT ["node", "-r", "sucrase/register", "app.js"]