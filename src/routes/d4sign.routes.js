import { Router } from 'express';
import D4signController from '../app/controllers/D4signController';
import formidableMiddleware from 'express-formidable';

const d4signRoutes = Router();

/**
* swagger
* /d4sign/get:
*  get:
*    summary: Status de documento na D4sign.
*    description: Usando a API da d4sign, obtém se o documento já foi assinado. Também serve para forçar atualização do contrato se ocorreu alguma falha de comunicação com a D4Sign e ela não fez a requisição para avisar quando o contrato foi assinado.
*    tags:
*     - d4sign
*    requestBody:
*      content:
*        application/json:
*          schema:
*            type: object
*            properties:
*              uuid:
*                type: string
*                description: O uuid (id único) gerado pela d4sign para o documento.
*    responses:
*      200:
*        description: Status obtido e retornado.
*        content:
*          application/json:
*            schema:
*              type: object
*              properties:
*                assinado:
*                  type: boolean
*                  description: Se o contrato já foi assinado
*      400:
*        description: Bad Request.
*      403:
*        description: Token não encontrado.
*/
d4signRoutes.get('/get', D4signController.consultaAssinatura);
d4signRoutes.post('/signed', formidableMiddleware(), D4signController.webhookD4sign);


export default d4signRoutes;
