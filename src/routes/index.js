import { Router } from 'express';

import userRoutes from './user.routes';
import contractRoutes from './contract.routes';
import sessionRoutes from './session.routes';

import d4sign from './d4sign.routes';

const routes = Router();

routes.use('/user', userRoutes);
routes.use('/contratos', contractRoutes);
/* routes.use('/sessions', sessionRoutes); */
routes.use('/d4sign', d4sign);

module.exports = routes;
