import axios from 'axios'
import sqlServerUtils from '../../services/sqlserver';
import { format } from 'date-fns';
import { setD4SignStatus } from '../../services/d4sign';

import { getD4signDocument, getSignersEnvioAssinaturas } from "../middlewares/utils";

const INITIAL_TOKEN = process.env.INITIAL_TOKEN_D4SIGN_PROD;
const TOKEN_API = process.env.TOKEN_D4SIGN_PROD;
const CRYPT_KEY = process.env.CRYPT_KEY_D4SIGN_PROD;


const apiD4sign = axios.create({
  baseURL: "https://secure.d4sign.com.br/api/v1/",
  headers: {
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Headers": "*",
    "Access-Control-Allow-Methods": "*",

    "access-control-allow-origin": "*",
    "access-control-allow-headers": "*",
    "access-control-allow-methods": "*",

    "Content-Type": "application/json",
    "content-type": "application/json",
  },
});

class D4SignController {


  async envioAssinatura2(contract) {
    return new Promise(async (resolve, reject) => {
      const d4SignDocument = await getD4signDocument(contract);
      const d4SignSigners = getSignersEnvioAssinaturas(contract);
      const emailVendedor = contract.usuEmail;
      const usuarioVendedor = contract.usuUsuario;

      const contratoD4 = {
        documento: d4SignDocument,
        vendedor: {
          usuario: usuarioVendedor || null,
          email: emailVendedor || null,
        },
        signatarios: d4SignSigners
      }

      //D4SIGN NEW 

      //const { documento, signatarios, vendedor } = req.body;
      const { documento, signatarios, vendedor } = contratoD4;

      if (!documento) reject({ documento: null })
      if (!signatarios) reject({ signatarios: null })

      /* console.log("vendedor==>", JSON.stringify(vendedor), "<==vendedor"); */

      // 1) REGISTRO DOCUMENTO:
      let resCriacaoDocumento;
      try {
        resCriacaoDocumento = await apiD4sign.post(
          `documents/${INITIAL_TOKEN}/makedocumentbytemplate?tokenAPI=${TOKEN_API}&cryptKey=${CRYPT_KEY}`,
          documento
        );
      } catch (err) {
        console.log(err.response.data);
        reject('DOCUMENT_CREATE: ' + err.response.data.mensagem_pt || err.response.data.message || err.response.data.error || 'Erro desconhecido ao enviar documento');
        return;
      }
      /* console.log(
        "resCriacaoDocumento.data==>",
        resCriacaoDocumento.data,
        "<==resCriacaoDocumento.data"
      ); */
      //  data: { message: 'success', uuid: '268998e0-23ba-4d93-b81b-96c56a32e094' }

      if (!resCriacaoDocumento.data)
        reject({ criacaoDocumento: null })

      if (!resCriacaoDocumento.data.uuid)
        reject({ uuid: null })

      const { uuid } = resCriacaoDocumento.data;

      // 2) REGISTRO SIGNATARIOS
      let resCriacaoSignatarios;
      try {
        resCriacaoSignatarios = await apiD4sign.post(
          `documents/${uuid}/createlist?tokenAPI=${TOKEN_API}&cryptKey=${CRYPT_KEY}`,
          { signers: { ...signatarios } }
        );
      } catch (err) {
        reject('SIGNERS: ' + err.response.data.mensagem_pt || err.response.data.message || err.response.data.error || 'Erro desconhecido ao enviar documento');
        return;
      }


      /* console.log(
        "resCriacaoSignatarios==>",
        resCriacaoSignatarios.data,
        "<==resCriacaoSignatarios"
      ); */

      if (!resCriacaoSignatarios.status === 200)
        reject({ criacaoSignatarios: false })

      // 3) ENVIO DOCUMENTO:
      let resEnvioDocumento;
      try {
        resEnvioDocumento = await apiD4sign.post(
          `documents/${uuid}/sendtosigner?tokenAPI=${TOKEN_API}&cryptKey=${CRYPT_KEY}`,
          {
            message: "Segue documento para assinatura enviado pelo Aplicativo",
            workflow: "0",
            skip_email: "0",
          }
        );
      } catch (err) {
        reject('DOCUMENT_SEND: ' + err.response.data.mensagem_pt || err.response.data.message || err.response.data.error || 'Erro desconhecido ao enviar documento');
        return;
      }

      /* console.log(
        "resEnvioDocumento==>",
        resEnvioDocumento,
        "<==resEnvioDocumento"
      ); */

      if (!resEnvioDocumento.data)
        reject({ envioDocumento: null })

      if (!resEnvioDocumento.data.message)
        reject({ envioDocumento: false })


      //Devido a limitações na API da D4Sign, o cadastro de observadores funciona somente em produção.
      if (contratoD4.vendedor && contratoD4.vendedor.email !== undefined) {
        // 4) REGISTRA OBSERVADOR (VENDEDOR)
        let resVendedorObservador;
        try {
          resVendedorObservador = await apiD4sign.post(
            `watcher/${uuid}/add?tokenAPI=${TOKEN_API}&cryptKey=${CRYPT_KEY}`,
            {
              email: contratoD4.vendedor.email,
              permission: "1",
            }
          );
        } catch (err) {
          reject('OBSERVERS: ' + err.response.data.mensagem_pt || err.response.data.message || err.response.data.error || 'Erro desconhecido ao enviar documento');
          return;
        }

        if (!resVendedorObservador.data)
          reject({
            uuid,
            criacaoDocumento: true,
            criacaoSignatarios: true,
            envioDocumento: true,
            envioVendedor: null,
          })

        /* console.log(
          "resVendedorObservador==>",
          resVendedorObservador.data,
          "<==resVendedorObservador"
        ); */

        if (!resVendedorObservador.data.message)
          reject({
            uuid,
            criacaoDocumento: true,
            criacaoSignatarios: true,
            envioDocumento: true,
            envioVendedor: false,
          })
      }

      let resWebhook;
      try {
        resWebhook = await apiD4sign.post(
          `documents/${uuid}/webhooks?tokenAPI=${TOKEN_API}&cryptKey=${CRYPT_KEY}`,
          {
            //TODO: Inserir URL que receberá postback da Coobrastur no endpoint já criado /signed?uuid
            "url": `http://[URL_AQUI]/signed?${uuid}`
          }
        );
      } catch (err) {
        reject('WEBHOOK: ' + err.response.data.mensagem_pt || err.response.data.message || err.response.data.error || 'Erro desconhecido ao enviar documento');
        return;
      }

      if (!resWebhook.data.message)
        reject({ webhook: false })

      resolve({
        uuid,
        criacaoDocumento: true,
        criacaoSignatarios: true,
        envioDocumento: true,
        envioVendedor: true,
        webhook: true,
      })

    })
  }

  async webhookD4sign(req, res) {
    const { uuid, type_post } = req.fields;
    if (type_post == '1') {
      try {
        const res = await setD4SignStatus({ uuid, status: type_post });
        console.log(chalk.green(`[VENDA] Etapa 4 - Assinatura realizada`));
      } catch (err) {
        console.log('errstatus', err);
      }

    }
    res.sendStatus(200);
  }

  async consultaAssinatura(req, res) {
    const { uuid } = req.body;

    if (!uuid) return res.sendStatus(400);

    try {
      const resConsultaAssinaturas = await apiD4sign.get(
        `documents/${uuid}?tokenAPI=${TOKEN_API}&cryptKey=${CRYPT_KEY}`
      );

      if (!resConsultaAssinaturas)
        return res.send({ consultaAssinaturas: null }).status(400);

      if (!resConsultaAssinaturas.data)
        return res.send({ dataConsultaAssinaturas: null }).status(400);

      if (!resConsultaAssinaturas.data[0].statusId)
        return res.send({ statusId: null });

      const { statusId } = resConsultaAssinaturas.data[0];

      if (Number(statusId) !== 4) return res.send({ assinado: false });

      res.send({
        assinado: true,
      });

      setD4SignStatus({ uuid, status: statusId })

    } catch (e) {
      // console.log("resConsultaAssinaturas error: ", e);

      return res
        .send({ consultaAssinaturas: null, error: JSON.stringify(e.data) })
        .status(400);
    }
  }

}

export default new D4SignController();
